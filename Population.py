'''
Created on 20.12.2011

Population of Genomes

@author: pyka
'''
import Genome
import matplotlib.pyplot as plt
import numpy as np
import Cnpn_conf
import copy
import Species
import random
import pickle


class Population(object):
    '''
    This class represents a population of Genomes and provides all methods for crossover, mutation and selection
    '''


    def __init__(self, innovation=0):
        '''
        Constructor
        '''
        self.species = []
        self.species_id = -1
        #species = Species.Species()
        #self.species.append(species)
        
        ''' innovation counter, increases with each gene, that is added to a genome '''
        self.innovation = innovation
        
    def nextID(self):
        ''' returns the next species id '''
        self.species_id += 1
        return self.species_id 
        
        
    def initialize(self, num):
        ''' create the first genome and adopt the innovation counter '''
        g = Genome.Genome(1)
        species = Species.Species(g)
        species.id = self.nextID()
        self.species.append(species)
        
        self.innovation = g.counter
        
        #g.mutate_AddGene(self.nextInnovation())
        #g.mutate_AddGene(self.nextInnovation())
        #g.mutate_AddCis()
        #g.mutate_AddCis()
        #g.mutate_AddGene(self.nextInnovation())        
                
        for i in range(1, num):
            h = copy.deepcopy(g)
            #h.mutate_AddGene(self.nextInnovation())
            #h.mutate_AddCis()
            h.changeAllCisWeights()
            self.species[0].add(h)
            
    def nextInnovation(self):
        self.innovation += 1
        return self.innovation
    
    def similarityMatrix(self):
        ''' returns a similarity matrix for all genomes '''
        matrix = []
        for genome1 in self.species[0].genomes:
            list = []
            for genome2 in self.species[0].genomes:
                list.append(genome1.similarity(genome2))
            matrix.append(list)
        return matrix
            
            
    def plotGenomes(self, x, y, index=0, spec=0):
        ''' Plots all genomes. x and y denote the number of plots in rows and columns
        index denotes the output-variable, which should be plotted '''
        plt.figure();
        counter = 1
        for gen in self.species[spec].genomes:
            field = gen.showField(Cnpn_conf.display_area[0], Cnpn_conf.display_area[1], Cnpn_conf.display_area[2])
            plt.subplot(x, y, counter)
            plt.imshow(field, vmin=0, vmax=1)
            counter += 1
            
    def selectGenome(self, spec_num, parents):
        ''' selects a genome from one of the species '''
        return self.species[spec_num].genomes[random.choice(parents)]
        
    def addGenome(self, genome, tospecies = True):
        ''' Adds a genome to the population '''
        if (tospecies):
            ''' add it to a species or generate a new one '''
            res = Cnpn_conf.display_area[2]**2
            sims = []
            for species in self.species:
                sim = genome.similarity_phenotype(species.representant)
                #print("Population.addGenome - Distance is " + str(float(sim)/res)))
                if (float(sim)/res)>Cnpn_conf.sim_threshold_phenotype:
                    species.genomes.append(genome)
                    return
                sims.append(sim)
                
            ''' if no species with similarity to genome could be found, create a new one,
            but only, if the maximal number of species is not exceeded '''
            if (len(self.species) < Cnpn_conf.ml_max_species):
                species = Species.Species(genome)
                species.id = self.nextID()
                self.species.append(species)
            else:
                #self.species[-1].genomes.append(genome)
                self.species[sims.index(np.max(sims))].genomes.append(genome)
                
        else:
            ''' add the genome always to the first species '''
            ''' if there is no species, create one '''
            if (len(self.species) < 1):
                species = Species.Species(genome)
                self.species.append(species)
            else:
                ''' otherwise, use the first species '''
                self.species[0].genomes.append(genome)
                
        
    def exportGenome(self, number, filename):
        ''' Exports a genome as GraphViz '''
        self.species[0].genomes[number].exportGraph(filename)
        
    def save(self, filename):
        ''' saves the current generation '''
        f = open(filename, 'w')
        pickle.dump(self, f)
        f.close()        
            
    def nextGeneration(self, num, parents):
        ''' performs mutation and crossover to generate a next generation 
        num:     Number of offsprings
        parents: list with the numbers of genomes used for crossove and mutation
        '''
        ''' check whether we have genomes at all '''
        if (len(self.species[0].genomes) == 0):
            print("No individuals. Start with initialize()")
            return
        
        ''' generate num new offsprings '''
        newp = Population(self.innovation)
        for i in range(0, num):
            parent1 = self.selectGenome(0, parents)
            parent2 = self.selectGenome(0, parents)
            offspring = parent1.crossover(parent2)
           
            ''' changes randomly the function of the gen '''
            for gen_num in range(0, len(offspring.genlist)):
                if (random.random()<Cnpn_conf.mutation_genfunction):
                    offspring.mutate_GenFunction(gen_num)
                    
            ''' adds randomly a cis-element '''
            if (random.random()<Cnpn_conf.mutation_addcis):
                offspring.mutate_AddCis()
                
            ''' duplicates genes with a certain probability '''
            if (random.random()<Cnpn_conf.mutation_duplicategen):
                offspring.mutate_DuplicateGene()                
            
            ''' adds randomly a gen '''
            if (random.random()<Cnpn_conf.mutation_addgen):
                offspring.mutate_AddGene(self.nextInnovation())
                
            ''' changes randomly the weightings of cis-elements '''
            for cis_num in range(0, offspring.getCisNumber()):
                if (random.random()<Cnpn_conf.mutation_continues):
                    offspring.mutate_CisWeightContinues(cis_num)    
                    
            ''' clean genome '''
            for gen in offspring.detectSingleGenes():
                offspring.genlist.remove(gen)
                    
                
            newp.addGenome(offspring)
            
        newp.innovation = self.innovation
        return(newp)

    def imsave_representants(self, filename, dim=1):
        ''' saves an image with the representants of all species '''
        im = self.species[0].representant.showField(Cnpn_conf.display_area[0], Cnpn_conf.display_area[1], Cnpn_conf.display_area[2])
        if (len(self.species) > 1):
            for i in range(1, len(self.species)):
                im = np.concatenate((im, 
                                     self.species[i].representant.showField(Cnpn_conf.display_area[0], 
                                                                            Cnpn_conf.display_area[1], 
                                                                            Cnpn_conf.display_area[2])),
                                    dim)
        for i in range(len(self.species),Cnpn_conf.ml_max_species):
            im = np.concatenate((im,np.zeros((Cnpn_conf.display_area[2],Cnpn_conf.display_area[2])) ), dim )
                                 
        plt.imsave(filename, im, vmin=0, vmax=1)
        
  
        
        
    
'''p = Population(6)
p.initialize(16)
p2 = p.nextGeneration(16, [1, 1, 1, 1])

print(p.species[0].genomes[1].getValues(1, 0.5, 0))
print(p2.species[0].genomes[1].getValues(1, 0.5, 0))'''