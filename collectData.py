import analyseEvolvability as aE
import glob
import pickle

data_path = './OE-HyperNEAT/'

def evoAlongSpecies():
    liste = glob.glob(data_path + '/r*')
    for folder in liste:
        print(folder)
        c, e = aE.plotEvoAlongSpecies(folder)
        f = open(folder + '_evo_along_species.equi', 'wb')
        pickle.dump([c, e], f)
        f.close()
        
