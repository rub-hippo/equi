# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 16:39:18 2013

@author: martin
"""

import pickle
from matplotlib.pyplot import *
import numpy as np
import collections

def loadData(filename):
    f = open(filename, 'r')
    data = pickle.load(f)
    f.close()
    
    return data

def flatten(x):
    ''' from: http://stackoverflow.com/questions/2158395/flatten-an-irregular-list-of-lists-in-python '''
    if isinstance(x, collections.Iterable):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]

def flatData(data):
    result = []
    for x in data:
        for y in x:
            result.append(y)
            
    return result

def evoMean(data):
    result = []
    for gen in data:
        result.append(np.mean(gen))
    return result
       
       
ion()     
regular = loadData('./regular/pop249.pop.evo')
#figure()
#hist(evoMean(regular))
regular = flatten(regular) 

random = loadData('./random/pop249.pop.evo')
#figure()
#hist(evoMean(random))
random = flatten(random)

regular_hist = np.histogram(regular, 100, [0., 1000.])
random_hist = np.histogram(random, 100, [0., 1000.])

regular_hist_n = regular_hist[0] / float(np.sum(regular_hist[0]))
random_hist_n = random_hist[0] / float(np.sum(random_hist[0]))

figure()
ax = subplot(111)
w = 0.3
x = np.array(range(100))
ax.bar(x-w, regular_hist_n,width=w,color='b',align='center')
ax.bar(x, random_hist_n,width=w,color='g',align='center')

ax.autoscale(tight=True)

show()
#plot(range(50), regular_hist_n)
#plot(range(50), random_hist_n)