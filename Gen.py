import ActiveFunction
from numpy.core.numeric import *
from random import *
import Cnpn_conf


'''
Created on 19.07.2011

@author: pyka
'''

class Gen(object):
    '''
    classdocs
    '''


    def __init__(self, id, func=ActiveFunction.gaussian, bias=0):
        '''
        Constructor, assigns the new gen an id (can be regarded as innovation number
        and an empty list of cis elements of the gen. Each element in the list consists of 
        (gen, bias, scale, weight), describing the input taken from another gen and its bias, scale and weight
        '''
        self.id = id
        self.cis = [];
        self.func = func                            # Activation Function
        
        self.bias = bias
        
        # Activation value of this Gen. If this value is "" it computes the real value by 
        # calling the other genes. When this value has already been computed it is used directly
        # => speed up trick
        self.value = 0
        
        # this variable is used for recurrence-checks. it stores which genes use this gene as
        # cis-element. if the same id appears twice, there must be a recursive loop in the net
        self.callers = []
        
        
    def getRandom(self):
        return gauss(Cnpn_conf.weight_range[0], Cnpn_conf.weight_range[1])
        
        
    def reset(self):
        '''
        Reset a gen: set value to "" and any visit tag to false. This function is used before 
        checking for recurrent connections or calculating gen-activation values
        '''
        self.value = 0
        self.callers = []
        
    def checkRecurrence(self, caller):
        ''' 
        Receives a gene id and checks whether the gene has already called this gene
        Returns false, if no recurrence could be detected
        ''' 
        if self.callers.count(caller) == 0:
            self.callers.append(caller)
            result = False
            for elem in self.cis:
                if elem[1] != 0.0:
                    result = result | elem[0].checkRecurrence(self.id)
            return result
        else:
            return True
                
        
    def getId(self):
        '''
        Returns the Id of the gen
        '''
        return self.id;
        
    def addCis(self, gen, bias, scale, weight):
        '''
        Adds a new cis-element to the list
        '''
        
        # if the list is empty, simply add the gen
        if len(self.cis)==0:
            self.cis.append([gen, bias, scale, weight])
            return
          
        # otherwise find the position before the first element
        # with a higher id            
        for x in range(0,len(self.cis)):
            if (self.cis[x][0].getId()>gen.getId()):
                self.cis.insert(x, [gen, bias, scale, weight])
                return
        
        # if there is no gen with a higher id, append the gen
        self.cis.append([gen, bias, scale, weight])
        
        
    def reSort(self):
        ''' resorts the last cis-element to its correct position, is used by Genome.mutation_AddCis '''
        cis = self.cis.pop()
        self.addCis(cis[0], cis[1], cis[2], cis[3])
        
    def getValue(self, caller):
        ''' 
        Returns the activation value of a gen by recursive calling other genes
        '''
        if (self.callers.count(caller) >= Cnpn_conf.iterations):
            return self.value
        else:
            self.callers.append(caller)
            self.value = 0.
            for elem in self.cis:
                self.value += self.func((elem[0].getValue(self.id) + elem[1]) * elem[2]) * elem[3]
                
            self.value += self.bias                

            if (self.value > 1.):
                self.value = 1.
            if (self.value < 0.):
                self.value = 0.

            return self.value
    
    def usedCis(self):
        ''' returns number of used cis-genes (weight not zero) '''
        #result = 0
        #for cis in self.cis:
            #if (cis[1] != 0.0):
        #    result += 1
        return len(self.cis) # result
        
    
    def setValue(self, value):
        '''
        Allows the manual setting of the gen-value
        '''
        self.value = value
        
    def removeGen(self, gen):
        '''
        Remove a gen from the cis-list
        '''
        for elem in self.cis:
            if elem[0] == gen:
                self.cis.remove(elem)
                return
            
         
        
'''        
g = Gen(1)
g1 = Gen(2)
g1.value = 0.3;
g2 = Gen(3)
g2.value = 0.4;
g.addCis(g1, 1)
g.addCis(g2, 0.5)

print(g.cis)'''
