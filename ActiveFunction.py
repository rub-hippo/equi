import math

'''
Created on 16.12.2011

Activation Functions for the CNPN

@author: pyka
'''

sqrt_pi_2 = 1 / math.sqrt(2 * math.pi)


''' the range of all f(x) is [-1 1] '''

def gaussian(value):
    # return sqrt_pi_2 * math.exp(-0.5 * (value**2))
    return math.exp(-0.5 * ((value*4.)**2)) 

def sigmoid(value):
    return (1 / (1+ math.exp(-4.9 * value)))

def sin(value):
    return math.sin(value * math.pi)

def identity(value):
    return value

def threshold(value):
    if (value < 0.):
        return 0.
    else:
        return 1.
    
''' Special functions for output genes '''    
    
def coneangle(value):
    if (value > 0.9):
        return 0.9
    if (value < 0.05):
        return 0.05
    return value

''' applies the gaussian function and then decides whether there is a neuron or not '''

def neuron(value):
    return gaussian(value)
    #return threshold(gaussian(value))

af_list = [gaussian, sigmoid, sin, identity, threshold] 
af_number = 5