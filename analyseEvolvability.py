import ModelSearch
import Cnpn_conf
import numpy as np
import pickle
import collections
from matplotlib.pyplot import *
import glob
import scipy.stats


# Entropy related stuff: Stolen from Ero Carrera
# http://blog.dkbza.org/2007/05/scanning-data-for-entropy-anomalies.html
import math


g_rules = [[0, -1, -1, -1],    # for neutral
           [1, 0, 1, -1],    # for stone
           [1, -1, 0, 1],    # for scissor
           [1, 1, -1, 0]]    # for paper



''' measure evolvability for the first population '''

def diffFields(f1, f2):
    return np.sum(f1 != f2)
    
def flatten(x):
    if isinstance(x, collections.Iterable):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]


def analyseEvolvability(filename):
    
    i = 0
    
    f = open(filename, 'rb')
    m = pickle.load(f)
    f.close()
    
    evolve = []
    for s in m.p.species:
        speciesevo = []
        print('Next Species')
        for g in s.genomes:
            i += 1 
            print('Next Genome: ' + str(i))

            orig = g.showField(Cnpn_conf.display_area[0],
                               Cnpn_conf.display_area[1],
                               Cnpn_conf.display_area[2])
            genomeevo = []
            for gen in g.genlist:
                genevo = []
                for cis in gen.cis:
                    for c in range(1,4):
                        cis[c] = cis[c] + Cnpn_conf.continues_change[1]
                        deriv = g.showField(Cnpn_conf.display_area[0],
                                            Cnpn_conf.display_area[1],
                                            Cnpn_conf.display_area[2])
                        diff = diffFields(orig, deriv)
                        
                        cis[c] = cis[c] - 2*Cnpn_conf.continues_change[1]
                        deriv = g.showField(Cnpn_conf.display_area[0],
                                            Cnpn_conf.display_area[1],
                                            Cnpn_conf.display_area[2])
                        
                        genevo.append(float(diff + diffFields(orig, deriv)) / 2)
                        cis[c] = cis[c] + Cnpn_conf.continues_change[1]
                genomeevo.append(genevo)

            speciesevo.append(genomeevo)
            print(speciesevo)
        evolve.append(speciesevo)
            
        f = open(filename+'.evo', 'wb')
        pickle.dump(evolve, f)
        f.close()
        
        
def analyseEvoOfFolder(folder, start=0, stepsize=10, end=250):
    ''' calls analyseEvolvability for all files in a given folder and a certain
    stepsize '''
    liste = []
    for x in range(end):
        liste.append(folder + "/pop%03d.pop" % x)
    
    for f in liste[start::stepsize]:
        analyseEvolvability(f)

        
def loadData(filename):
    f = open(filename, 'r')
    data = pickle.load(f)
    f.close()
    
    return data

def analyseFitnessChange(filename):
    ''' For each change in each cis-element, the function returns a vector that
    denotes the change in fitness to the other individuals. With this function,
    we can see, how species-specific certain changes in certain cis-elements are
    '''
    m = loadData(filename)
    m.collectAllGenomes()
    phenos = []
    
    for genome in m.genomelist[1:]:
        phenos.append(genome.getFieldSymbols(
                                            Cnpn_conf.display_area[0], 
                                            Cnpn_conf.display_area[1], 
                                            Cnpn_conf.display_area[2]))
    
    genome = m.genomelist[0]
    genome_pheno = genome.getFieldSymbols(Cnpn_conf.display_area[0],
                                          Cnpn_conf.display_area[1],
                                          Cnpn_conf.display_area[2])
    
    battle_values = []
    for p in phenos:
        battle_values.append(m.battle(genome_pheno, p))
    
    battle_values = np.array(battle_values)
    
    change = []
    i = 0
    for gen in genome.genlist:
                i += 1
                print(str(i) + '/' + str(len(genome.genlist)))
                for cis in gen.cis:
                    for c in range(1,4):
                        cis[c] = cis[c] + Cnpn_conf.continues_change[1]
                        deriv = genome.getFieldSymbols(Cnpn_conf.display_area[0],
                                                       Cnpn_conf.display_area[1],
                                                       Cnpn_conf.display_area[2])
                        
                        genome_change = []
                        for p in phenos:
                            genome_change.append(m.battle(deriv, p))
                        
                        change.append(np.array(genome_change) - battle_values)
                        
                        cis[c] = cis[c] - Cnpn_conf.continues_change[1]
    return change
        
def analyseGenomes(data):
    ''' Returns the evolvability of genomes '''
    evolves = []
    for s in data:
        for g in s:
            genome = flatten(g)
            evolves.append(np.mean(genome))
            
    return evolves

def analyseSpecies(data):
    ''' Returns the evolvability of genomes '''
    evolves = []
    for s in data:
        genomes = []
        for g in s:
            genome = np.array(flatten(g))
            genomes.append(np.mean(genome))
            #genomes.append(np.sum(genome))  # following the example by Lehmann et al. 
            #if (len(genome[genome>0]) == 0):
            #    genomes.append(0)
            #else:
            #    #genomes.append(np.mean(genome[genome>0]))
            #    genomes.append(float(len(genome[genome>0]))/len(genome))
                
        evolves.append(np.mean(genomes))
            
    return evolves        
                
def plotEvoOfGenomes(folder):
    liste = sorted(glob.glob(folder + '/*evo'))
    result = []
    xaxis = []
    
    ygenomes = []
    xgenomes = []
    
    for evo in liste:
        print(evo)
        data = loadData(evo)
        evoGenomes = analyseGenomes(data)
        result.append(np.mean(evoGenomes))
        xvalue = int(evo[(2+len(folder)+4):(2+len(folder)+7)])
        xaxis.append(xvalue)
        
        for g in evoGenomes:
            ygenomes.append(g)
            xgenomes.append(xvalue)
    
    plot(xaxis, result)
    #plot(xgenomes, ygenomes, '*')    
    
    
def plotEvoOfSpecies(folder, plot_data=True):
    liste = sorted(glob.glob(folder + '/*evo'))
    yspec = []
    xspec = []
    
    yspecs = []
    xspecs = []
    
    ymin = []
    ymax = []
    
    for evo in liste:
        data = loadData(evo)
        # Determine evolvability of all species
        evoSpecies = analyseSpecies(data)
        # Determine x-value
        xvalue = int(evo[(len(folder)+4):(len(folder)+7)])

        print(evo + ': ' + str(np.mean(evoSpecies)) + '  l: ' + str(len(evoSpecies)))
        
        yspec.append(np.mean(evoSpecies))
        xspec.append(xvalue)
        ymin.append(np.min(evoSpecies))
        ymax.append(np.max(evoSpecies))
        for e in evoSpecies:
            yspecs.append(e)
            xspecs.append(xvalue)
    
    if plot_data:
        #plot(xspecs, yspecs, '*')        
        plot(xspec, yspec, 'g', linewidth = 4)
        
    return yspec, xspec, ymin, ymax

    
    
def getNumOfSpecies(folder):
    ''' returns the number of species for each generation '''
    result = []
    liste = sorted(glob.glob(folder + '/*pop'))
    for x in range(len(liste)):
        filename = liste[x]
        f = open(filename, 'rb')
        m = pickle.load(f)
        f.close()

        print(filename  + ': ' + str(len(m.p.species)))
        result.append(len(m.p.species))
    return result
    
    
def imSaveOfSpecies(folder):    
    ''' save the evolution of a species across generations by saving its representant for
    each generation '''

    liste = sorted(glob.glob(folder + '/*pop'))
    
    ''' what is the highest species-id-number? '''
    max_id = 0;
    f = open(liste[-1], 'rb')
    m = pickle.load(f) 
    f.close()
    for s in m.p.species:
        max_id = max(max_id, s.id)
    

    occurs_in = [[] for i in range(max_id+2)]
    at_position = [[] for i in range(max_id+2)]
    ''' first, look, in which files this species occurs '''
    for f in liste[:-1]:
        print('Check ' + f)
        fi = open(f, 'rb')
        m = pickle.load(fi) 
        fi.close()
        for y in range(0,len(m.p.species)):
            occurs_in[m.p.species[y].id].append(f)
            at_position[m.p.species[y].id].append(y)    
    
    # go through each id
    for x in range(0,max_id+1):
        # determine the number of generations in which this species occured
        elems = len(occurs_in[x])
        if elems > 0:
            print('Write images for species' + str(x))
            feld = np.zeros((Cnpn_conf.display_area[2]*elems, Cnpn_conf.display_area[2]))

            for i in range(0, len(occurs_in[x])):
                print('Write image ' + str(i) + '/' + str(len(occurs_in[x])))
                f = open(occurs_in[x][i], 'rb')
                m = pickle.load(f)
                f.close()
                
                # take the second genome and not the representant to display slower
                # changes
                field = m.p.species[at_position[x][i]].genomes[0].showField(Cnpn_conf.display_area[0], 
                                                                              Cnpn_conf.display_area[1], 
                                                                              Cnpn_conf.display_area[2])
                feld[(i*Cnpn_conf.display_area[2]):((i+1)*Cnpn_conf.display_area[2]),
                     0:Cnpn_conf.display_area[2]] = field.transpose()
        
            matplotlib.pyplot.imsave(folder + "/species%03d" % x + ".png", feld, vmin=0, vmax=1)
            
def imSaveOfOneSpeciesChange(folder, spec_id):
    ''' saves the representant and all members of a species with a given id across all generations'''
    liste = sorted(glob.glob(folder + '/*pop'))
    for x in range(len(liste)):
        filename = liste[x]
        print(filename)
        f = open(filename, 'rb')
        m = pickle.load(f)
        f.close()
        
        found = False
        for s in m.p.species:
            if s.id != spec_id:
                continue
            
            found = True
            feld = np.zeros((Cnpn_conf.display_area[2]*(len(s.genomes)+1), Cnpn_conf.display_area[2]))
            field = s.representant.showField(Cnpn_conf.display_area[0], 
                                             Cnpn_conf.display_area[1], 
                                             Cnpn_conf.display_area[2])
            feld[(0*Cnpn_conf.display_area[2]):((0+1)*Cnpn_conf.display_area[2]),
                                 0:Cnpn_conf.display_area[2]] = field                                                                          

            i = 1
            for g in s.genomes:
                field = g.showField(Cnpn_conf.display_area[0], 
                                    Cnpn_conf.display_area[1], 
                                    Cnpn_conf.display_area[2])
                feld[(i*Cnpn_conf.display_area[2]):((i+1)*Cnpn_conf.display_area[2]),
                     0:Cnpn_conf.display_area[2]] = field
                i += 1
            
        if found:
            matplotlib.pyplot.imsave(folder + "/species%03d_%03d" % (spec_id, x) + ".png", feld, vmin=0, vmax=1)
            
        
            
def imSaveOfSpeciesChange(folder):    
    ''' save the evolution of a species across generations by saving its representant for
    each generation '''

    liste = sorted(glob.glob(folder + '/*pop'))
    
    ''' what is the highest species-id-number? '''
    max_id = 0;
    f = open(liste[-1], 'rb')
    m = pickle.load(f) 
    f.close()
    for s in m.p.species:
        max_id = max(max_id, s.id)
        
    max_id = max_id + 2        
    
    occurs_in = [[] for i in range(max_id+1)]
    at_position = [[] for i in range(max_id+1)]
    ''' first, look, in which files this species occurs '''
    for f in liste[:-1]:
        print('Check ' + f)
        fi = open(f, 'rb')
        m = pickle.load(fi) 
        fi.close()
        for y in range(0,len(m.p.species)):
            occurs_in[m.p.species[y].id].append(f)
            at_position[m.p.species[y].id].append(y)    
            
    feld = np.zeros((max_id, len(liste)))    
    
    # go through each id
    for x in range(0,max_id+1):
        # determine the number of generations in which this species occured
        elems = len(occurs_in[x])
        if elems > 1:
            f = open(occurs_in[x][0], 'rb')
            m = pickle.load(f)
            f.close()
                
            field1 = m.p.species[at_position[x][0]].representant.showField(Cnpn_conf.display_area[0], 
                                                                           Cnpn_conf.display_area[1], 
                                                                           Cnpn_conf.display_area[2])
            
            for i in range(1, len(occurs_in[x])):
                print('Analyse image ' + str(i) + '/' + str(len(occurs_in[x])))
                f = open(occurs_in[x][i], 'rb')
                m = pickle.load(f)
                f.close()
                
                field2 = m.p.species[at_position[x][i]].representant.showField(Cnpn_conf.display_area[0], 
                                                                               Cnpn_conf.display_area[1], 
                                                                               Cnpn_conf.display_area[2])
                
                # compute the difference between the previous and the current generation
                difference = np.sum(field1 != field2) 
                # determine the generation number in which this difference occured
                xvalue = int(occurs_in[x][i][(len(folder)+4):(len(folder)+7)])                                                              
                # write pixel value 
                feld[x, xvalue] = difference + 100
                field1 = field2
            
            matplotlib.pyplot.imsave( folder + "/species_change_all.png", feld, vmin=0, vmax=(Cnpn_conf.display_area[2]**2)/2)            
    
    return feld
            
    
def Correlation_EvoSize(filename):
    ''' Measures the correlation between species size and evolvability 
    for one generation '''
    data = loadData(filename)
    spec_evo = analyseSpecies(data)
    spec_size = []
    for s in data:
        spec_size.append(len(s))
    corr = np.corrcoef(spec_size, spec_evo)
    return corr, spec_evo, spec_size
    
def Correlation_EvoSize_All(folder):
    ''' Measures the correlation between species size and evolvability 
    for all generations '''
    liste = sorted(glob.glob(folder + '/*evo'))
    result = []
    
    for evo in liste:
        corr = Correlation_EvoSize(evo)
        result.append(corr[0][0][1])
        print(evo + ': ' + str(corr[0][0][1]))
    
    return result
    
def plotEvoAlongSpecies(folder):
    ''' plots evolvability values along species '''
    liste = sorted(glob.glob(folder + '/*evo'))
    
    ''' what is the highest species-id-number? '''
    max_id = 0;
    f = open(liste[-1][:-4], 'rb')
    m = pickle.load(f) 
    f.close()
    for s in m.p.species:
        max_id = max(max_id, s.id)
    

    occurs_in = [[] for i in range(max_id+1)]
    at_position = [[] for i in range(max_id+1)]
    ''' first, look, in which files this species occurs '''
    for f in liste[:95]:
        print('Check ' + f)
        fi = open(f[:-4], 'rb')
        m = pickle.load(fi) 
        fi.close()
        for y in range(0,len(m.p.species)):
            occurs_in[m.p.species[y].id].append(f)
            at_position[m.p.species[y].id].append(y)

    corrs = []   # correlation between species size and evolvability
    evo_length = [[],[]] # correlation between no. of generations of a species and its average evolvability

    #figure()

    for x in range(0,max_id+1):
        
        xdata = []
        ydata = []   # this is for evolvability
        ydata2 = []  # this is for size of species
        for i in range(0, len(occurs_in[x])):
            fi = open(occurs_in[x][i], 'rb')
            e = pickle.load(fi)
            fi.close()
            
            xvalue = int(occurs_in[x][i][(len(folder)+4):(len(folder)+7)])
            xdata.append(xvalue)

            yvalues = []            
            if len(e) > at_position[x][i]:
                for g in e[at_position[x][i]]:
                    genome = flatten(g)
                    yvalues.append(np.mean(genome))
                ydata2.append(len(e[at_position[x][i]]))
            else:
                yvalues = -1
                ydata2.append(0)
            
            ydata.append(np.mean(yvalues))
        
        if len(xdata)>=10:
            evo_length[0].append(xdata[-1] - xdata[0])
            evo_length[1].append(np.mean(ydata))
            corr = np.corrcoef(ydata[3:], ydata2[3:])
            #figure()
            #plot(ydata[3:])
            #plot(ydata2[3:])
            
            #subplot(2,1,1)            
            plot(xdata, ydata, color='0.75')
            #subplot(2,1,2)
            #plot(xdata,ydata2)
            
            print(str(x) + ': ' + str(corr[0][1]) + '   l: ' + str(len(xdata)))
            corrs.append(corr[0][1])
        else:
            print(x)
            
    return corrs, evo_length
    
def countGenes(filename):
    ''' count the number of genes per genome in a given population '''
    f = open(filename, 'rb')
    m = pickle.load(f)
    f.close()
    
    genes = []
    
    for s in m.p.species:
        genes_spec = []
        for g in s.genomes:
            genes_spec.append(len(g.genlist))
            genes.append(len(g.genlist))
        print('Species ' + str(s.id) + ': ' + str(np.mean(genes_spec)) + '  l: ' + str(len(genes_spec)) + ' f: ' + str(s.fitness))
        
    return genes
    

def countCis(filename):
    ''' count the number of cis-elements per gen in a given population '''
    f = open(filename, 'rb')
    m = pickle.load(f)
    f.close()
    
    cis = []
    
    for s in m.p.species:
        cis_spec = []
        for g in s.genomes:
            for gen in g.genlist:
                cis_spec.append(len(gen.cis))
                cis.append(len(gen.cis))
        print('Species ' + str(s.id) + ': ' + str(np.mean(cis_spec)) + '  l: ' + str(len(cis_spec)) + ' f: ' + str(s.fitness))
        
    return cis    



def meanGenes(folder):
    liste = sorted(glob.glob(folder + '/*pop'))

    result = []    
    full = []
    for pop in liste:
        genes = countGenes(pop)
        print(pop + ": " + str(np.mean(genes)))
        result.append(np.mean(genes))
        full.append(genes)
    
    return result, full

def meanCis(folder):
    liste = sorted(glob.glob(folder + '/*pop'))

    result = []    
    for pop in liste:
        cis = countCis(pop)
        print(pop + ": " + str(np.mean(cis)))
        result.append(np.mean(cis))
    
    return result            
    

def range_bytes (): return range(4)

def H(data, iterator=range_bytes):
    # measures the entropy of a given image
    data = flatten(data);
    if not data:
        return 0
        
    entropy = 0
    for x in iterator():
        p_x = float(data.count(x))/len(data)
        if p_x > 0:
            entropy += - p_x*math.log(p_x, math.e)
                
    return entropy


def plotEntropyOfSpecies(folder):
    liste = sorted(glob.glob(folder + '/*pop'))

    entropy = []    
    mean_entr = [];  
    
    diffs = []
    mean_diffs = []    
    
    xvalues = []
    xval = -1
    for filename in liste[0::10]:
        print(filename)
        f = open(filename, 'rb')
        m = pickle.load(f)
        f.close()
        
        pop_entr = []
        pop_diffs = []
        xval += 1
        for species in m.p.species:
            p = species.representant.getFieldSymbols(Cnpn_conf.display_area[0], 
                                                     Cnpn_conf.display_area[1],
                                                     Cnpn_conf.display_area[2])
            pop_entr.append(H(p))
            pop_diffs.append(np.sum(np.diff(p) != 0))
            xvalues.append(xval)            
            
        entropy.append(pop_entr)
        diffs.append(pop_diffs)
        
        mean_entr.append(np.mean(pop_entr));
        mean_diffs.append(np.mean(pop_diffs))

    figure()        
    plot(xvalues, flatten(entropy), '*') 
    figure()
    plot(xvalues, flatten(diffs), '*') 
    
    return entropy, mean_entr, diffs, mean_diffs

def countSpeciesSize(folder):
    liste = sorted(glob.glob(folder + '/*pop'))

    spec1 = []
    spec2 = []
    for filename in liste:
        print(filename)
        f = open(filename, 'rb')
        m = pickle.load(f)
        f.close()
        spec1.append(len(m.p.species[0].genomes))
        if (len(m.p.species)>1):
            spec2.append(len(m.p.species[1].genomes))
        else:
            spec2.append(0)
       
    return spec1, spec2
    
def plotFitness(folder):
    ''' plots species-wise and individual-wise fitness '''
    liste = sorted(glob.glob(folder + '/*pop'))
    spec_wise = [];
    ind_wise = [];
    for filename in liste:
        print(filename)
        f = open(filename, 'rb')
        m = pickle.load(f)
        f.close()
        
        spec_fit = 0
        ind_fit = 0
        for s in m.p.species:
            spec_fit = spec_fit + s.fitness
            ind_fit = ind_fit + s.fitness * len(s.genomes)
        
        spec_fit = spec_fit / len(m.p.species)
        if spec_fit > 1000000:
            spec_fit = spec_fit / 2 # just for pop216, as it was evaluated twice
            
        spec_wise.append(spec_fit)
        
        ind_fit = ind_fit / len(m.genomelist)
        if ind_fit > 1000000:
            ind_fit = ind_fit / 2 # just for pop216, as it was evaluated twice
        
        ind_wise.append(ind_fit)
    
    plot(ind_wise)    
    plot(spec_wise)
    return ind_wise, spec_wise

    
def imSave_SpeciesRepresentants(folder):
    ''' saves for each generation all representants of each species '''    
    liste = sorted(glob.glob(folder + '/*pop'))
    for x in range(len(liste)):
        filename = liste[x]
        print(filename)
        f = open(filename, 'rb')
        m = pickle.load(f)
        f.close()
        m.p.imsave_representants(folder + "/generation%03d" % x + ".png")
        
    
def getSpeciesChronology(folder):    
    ''' returns two arrays providing for each species the information in which
    generation the species occurs and at which index in the species list'''

    liste = sorted(glob.glob(folder + '/*pop'))
    
    ''' what is the highest species-id-number? '''
    max_id = 0;
    f = open(liste[-1], 'rb')
    m = pickle.load(f) 
    f.close()
    for s in m.p.species:
        max_id = max(max_id, s.id) + 1
    

    occurs_in = [[] for i in range(max_id+1)]
    at_position = [[] for i in range(max_id+1)]
    ''' first, look, in which files this species occurs '''
    for f in liste[:-1]:
        print('Check ' + f)
        fi = open(f, 'rb')
        m = pickle.load(fi) 
        fi.close()
        for y in range(0,len(m.p.species)):
            occurs_in[m.p.species[y].id].append(f)
            at_position[m.p.species[y].id].append(y)    
            
    return occurs_in, at_position    
    
    
    
def collectAllGenomes(m):
    """ writes all genomes from all species into one list,
    taken from ModelSearch.py """
    genomelist = []
    for species in m.p.species:
        for genome in species.genomes:
            genomelist.append(genome)
    
    return genomelist
            


def battle(p1, p2):
    """ determines the fitness of both genomes according
    to their fitness in the phenotype """
    
    fit1 = 0 # fitness of g1 (for g2 it is -fit1)
    for x in range(Cnpn_conf.display_area[2]):
        for y in range(Cnpn_conf.display_area[2]):
            ''' get the list of values for a particular coordinate '''
            #s1 = [p1[g_neutral, x, y], p1[g_stone, x, y], p1[g_scissor, x, y], p1[g_paper, x, y]]      
            #s2 = [p2[g_neutral, x, y], p2[g_stone, x, y], p2[g_scissor, x, y], p2[g_paper, x, y]]
            ''' apply the game-rules to the substrates with the highest value ''' 
            fit1 = fit1 + g_rules[p1[x, y]][p2[x, y]]
    
    ''' returns the minimum because this is needed to adjust later on all fitness values '''
    return fit1


def asssesRandomFitness(samples):
    ''' generates random phenotypes and measures the fitness between pairs
    to assess the distribution of random fitness '''
    result = []
    for i in range(samples):
        print(i)
        result.append(
            battle(
                np.floor(
                    np.random.random(
                        (Cnpn_conf.display_area[2],Cnpn_conf.display_area[2])
                    )*3
                ).astype(int),
                np.floor(
                    np.random.random(
                        (Cnpn_conf.display_area[2],Cnpn_conf.display_area[2])
                    )*3
                ).astype(int)
            )
        )
    return result

    
def createBattleMatrix(filename):
    """ creates a battle-matrix by for all genomes listed in the
    population. Code were taken from ModelSearch.py and modified """
        
    f = open(filename, 'rb')
    m = pickle.load(f)
    f.close()        
        
    ''' collect all genomes to operate on the list of genomes directly '''
    genomelist = collectAllGenomes(m)
    
    ''' battle-matrix to be filled '''
    g_matrix = np.zeros((len(genomelist), len(genomelist)))
    
    ''' collect all phenotypes '''
    phenos = []

    print("Create all phenotypes")
    for i, genome in enumerate(genomelist):
        print(str(i) + '/' + str(len(genomelist)))
        phenos.append(genome.getFieldSymbols(
                                            Cnpn_conf.display_area[0], 
                                            Cnpn_conf.display_area[1], 
                                            Cnpn_conf.display_area[2]))
    
    print("Create battle-matrix")
    for x in range(0, len(genomelist)-1):
        print(x)
        
        opponents = range(x+1, len(genomelist))
        for y in opponents:
            fit1 = battle(phenos[x], phenos[y])
            g_matrix[x, y] = fit1
            g_matrix[y, x] = -fit1
            
    ''' create battle-matrix for species '''            
    spec_lengths = [len(s.genomes) for s in m.p.species]
    s_matrix = np.zeros((len(m.p.species), len(m.p.species)))
    row_start = 0
    
    for x, r_spec in enumerate(spec_lengths):
        
        col_start = 0
        for y, c_spec in enumerate(spec_lengths):
            s_matrix[x, y] = np.mean(
                g_matrix[row_start:(row_start+r_spec), col_start:(col_start+c_spec)])
            s_matrix[y, x] = np.mean(
                g_matrix[col_start:(col_start+c_spec), row_start:(row_start+r_spec)])
            col_start += c_spec
            
        row_start += r_spec
    
    return g_matrix, s_matrix

def createBattleMatrixForFile(filename, folder):
    ''' Create genome-wise and species-wise battlematrices and save them in 
    folder. Furthermore, return both matrices '''
    g_matrix, s_matrix = createBattleMatrix(filename)
    matplotlib.pyplot.imsave(folder + "/" + '_battle_spec' + ".png", s_matrix, 
                                 vmin=-1600, 
                                 vmax=1600)
    matplotlib.pyplot.imsave(folder + "/" + 'battle_gen' + ".png", g_matrix, 
                                 vmin=-1600, 
                                 vmax=1600)
    return g_matrix, s_matrix

def createBattleMatrixForFolder(folder):
    liste = sorted(glob.glob('./' + folder + '/*pop'))
    result = []
    for x in range(0,len(liste),5):
        filename = liste[x]
        print(filename)
        g_matrix, s_matrix = createBattleMatrix(filename)
        #s_matrix[(s_matrix < 800) & (s_matrix > -800)] = 0
        result.append(s_matrix)
        matplotlib.pyplot.imsave(folder + "/battle_full_matrix%03d" % x + ".png", s_matrix, 
                                 vmin=-1600, 
                                 vmax=1600)
        f = open(folder + '/battles_species.equi', 'wb')
        pickle.dump(result, f)
        f.close()
    
                                 
def analysePredPreyRelations(folder, threshold):
    """ Computes the predator-prey-relationships in any battlematrix that can e found in
    battles_species.equi of the population folder. Threshold defines the positive and negative
    threshold that the data points in each row have to exceed """

    f = open(folder + '/battles_species.equi', 'rb')
    data = pickle.load(f)
    f.close()
    
    result = []
        
    for i in range(len(data)):
        r = sum(((sum(data[i] > threshold)>0) & (sum(data[i] < -threshold)>0))) / float(len(data[i]))
        result.append(r)
    
    return result
         
def analysePPVarEvoForId(folder, id):
    ''' Measures the correlation between the variance in the predator-prey 
    matrix on the species level and the evolvability of the species. id refers
    to the pop and evo-file in a given folder '''
    pop_file = folder + 'pop%03d.pop' % id
    evo_file = folder + 'pop%03d.pop.evo' % id
    return analysePPVarEvo(pop_file, evo_file)
    
    
def analysePPVarEvo(pop_file, evo_file):
    m = loadData(pop_file)
    evo_data = loadData(evo_file)
    if len(m.p.species) != len(evo_data):
        print("Model-data and evo-data don't have the same number of species")
        return 0
    
    _, sm = createBattleMatrix(pop_file)
    
    evo = analyseSpecies(evo_data)
    
    return np.corrcoef(np.var(sm,0), evo)[0][1], np.var(sm, 0)

def analysePPVarEvoFolder(folder, id_list):
    ''' Measures the correlation between predator-prey variance and evolvability
    for a list of generations (given by a list of ids) and a given folder '''
    result = []
    for id in id_list:
        result.append(analysePPVarEvo(folder, id))
    return result

def analysePPVarEvoCompleteFolder(folder):
    ''' Measures correlation between predator-prey variance and evolvability for
    every evo-file in a folder '''
    result = []
    liste = sorted(glob.glob(folder + '/*evo'))
    for item in liste:
        corr = analysePPVarEvo(item[:-4], item)
        print(corr)
        result.append(corr)
    return result



def readBattleVar(filename):
    f = open(filename, 'r')
    data = []
    for line in f:
        d = line.split(',')
        for n in d[:-1]:
            data.append(float(n))
    return data

''' TODO:
    Measure the correlation between species size and degree of change to the next generation '''
