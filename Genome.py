'''
Created on 25.03.2012

The Genome class holds a collection of Gens

@author: pyka
'''

import numpy as np
from Gen import Gen
import ActiveFunction
from random import *
from gvgen import *
import Cnpn_conf
import copy
import pickle
import matplotlib
from time import time
import os

class Genome(object):

    def __init__(self, id):
        '''
        Generates all necessary attributes for the genome:
            genlist : list of all genes
        '''
        self.genlist = []
        
        ''' counter for genes '''
        self.counter = -1
        
        self.fitness = 0
        
        ''' Generate input genes '''
        self.x = Gen(self.nextGenId(), ActiveFunction.identity, 0)      # x-coordinate
        self.y = Gen(self.nextGenId(), ActiveFunction.identity, 0)      # y-coordinate
        self.d = Gen(self.nextGenId(), ActiveFunction.identity, 0)      # Distance to (0, 0, 0)
        self.b = Gen(self.nextGenId(), ActiveFunction.identity, 1)      # bias-coordinate
        
        self.inputs = [self.x, self.y, self.d, self.b]
     
        ''' Generate output genes '''
        self.n = Gen(self.nextGenId(), ActiveFunction.identity, 0)      # neutral-output
        self.s = Gen(self.nextGenId(), ActiveFunction.identity, 0)      # stone-output
        self.c = Gen(self.nextGenId(), ActiveFunction.identity, 0)      # scissor-output
        self.p = Gen(self.nextGenId(), ActiveFunction.identity, 0)      # paper-output

        ''' Labels that can replace the numbers in the dot-graph '''
        self.labels = ['x', 'y', 'd', 'b', 'n', 's', 'c', 'p']
        
        self.outputs = [self.n, self.s, self.c, self.p]

        for gen in self.outputs:
            self.genlist.append(gen)
            
        self.initialize()
        
    def save(self, filename):
        ''' saves the genome in a file '''
        f = open(filename, 'wb')
        pickle.dump(self, f)
        f.close()
        
    def saveField(self, filename, factor, translation, width):
        ''' saves the whole 3d-field with all data in a file that can be loaded in blender '''
        
        ''' first, allocate the memory '''
        data = np.zeros((width, width, width, len(self.outputs)))
        
        ''' then iterate through the whole field '''
        for x in range(0,width):
            for y in range(0,width):
                for z in range(0, width):
                    values = self.getValues(float(x)/(width-1) * factor + translation, float(y)/(width-1) * factor + translation, float(z)/(width-1) * factor + translation)
                    data[x][y][z] = np.array(values)
                    
        ''' open the file in binary mode. this is important, as blender uses another python-version '''
        f = open(filename, 'wb')
        
        ''' dump everything and close the file '''
        pickle.dump(data.tolist(), f)
        f.close()
        return data
    
    def save2DField(self, filename, factor, translation, width, index=0):
        ''' saves a 2d-image as image-file '''
        field = self.getField(factor, translation, width, index)
        matplotlib.image.imsave(filename, field)
    
    def saveVolume(self, filename, factor, translation, width, index):
        ''' saves the whole 3d-value as raw 8-bit values that can be displayed 
        with blenders volumetric renderer '''
        i = [0]*(width ** 3)

        ''' then iterate through the whole field '''
        for x in range(0,width):
            for y in range(0,width):
                for z in range(0, width):
                    values = self.getValues(float(x)/(width-1) * factor + translation, float(y)/(width-1) * factor + translation, float(z)/(width-1) * factor + translation)
                    v = values[index]
                    if (v > 1.0):
                        v = 1.0
                    if (v < 0.0):
                        v = 0.0
                    
                    i[x*(width**2) + y*width + z] = int(v * 255)
        
        f = open(filename, 'wb')
        f.write(bytearray(i))
        f.close()
        return i
                
    def adoptGenList(self, genlist):
        ''' If crossover has been made, output genes have to be remapped '''
        self.genlist = genlist
        self.outputs = self.genlist[0:len(self.outputs)]
        ''' replace the pointer to the genes '''
        # for each gen in genlist
        for gen in self.genlist:
            # go through each cis
            x = 0
            while x < len(gen.cis):
                cis = gen.cis[x]
                found = False
                # and if an input gen can be found
                for ingen in self.inputs:
                    # replace it
                    if (ingen.id == cis[0].id):
                        cis[0] = ingen
                        found = True
                        
                # check whether each cis-element refers to genes that exist
                for lgen in self.genlist:
                    if (lgen.id == cis[0].id):
                        cis[0] = lgen
                        found = True
                
                if found == False:
                    gen.cis.remove(cis)
                else:
                    x += 1
        
    def nextGenId(self):
        ''' 
        increments the gen-counter and returns the next valid id
        '''
        self.counter += 1
        return self.counter
    
    def getRandom(self, bias=0):
        return gauss(Cnpn_conf.weight_range[0], Cnpn_conf.weight_range[1])+bias
        
    def getRandomChange(self):
        return gauss(Cnpn_conf.continues_change[0], Cnpn_conf.continues_change[1])
        
    def initialize(self):
        '''
        Setup the default network configuration
        '''
        self.selectiveConnect(self.s, [self.x])
        self.selectiveConnect(self.c, [self.d])
        self.selectiveConnect(self.p, [self.y])
        self.selectiveConnect(self.n, [self.b])
        
    def selectiveConnect(self, gene, genlist):
        ''' connects all genes in genlist with the gene '''
        for input_gene in genlist:
            gene.addCis(input_gene, self.getRandom(), self.getRandom(1), self.getRandom(0.5))


    def fullConnect(self, gene):
        ''' connects one output gen with all inputs '''
        for input_gene in self.inputs:
            gene.addCis(input_gene, self.getRandom(), self.getRandom(1), self.getRandom())
        
                
    def reset(self):
        ''' 
        Resets all gen-activation values
        '''        
        for gen in self.inputs:
            gen.reset()
            
        for gen in self.genlist:
            gen.reset()
            

    def getValue(self, inputs, substrate):
        ''' computes only one value for given inputs '''
        self.reset()
        
        ''' assign all numbers of the input list to the given inputs '''
        for x in range(0, len(self.inputs)):
            self.inputs[x].bias = inputs[x]
        
        ''' return the requested value '''
        return self.outputs[substrate].getValue(-1)
        
                
    def getValues(self, vx, vy):
        '''
        Computes all output-values for the given genome
        '''
        
        ''' Resets all temporal values of all genes and sets
        values for input genes ''' 
        self.reset()
        self.x.bias = vx
        self.y.bias = vy
        self.d.bias = float(np.lib.scimath.sqrt((vx-0.5)**2 + (vy-0.5)**2))
        #self.b.bias = 1
        
        result = []
        
        ''' compute output of each output-gen '''
        for output_gen in self.outputs:
            self.reset()
            result.append(output_gen.getValue(-1))
        
        return result
    
    def getField(self, factor, translation, width, index=0):
        ''' Computes for a given range all values, the field is multiplied
        with factor and translated by the given parameter, weight refers to
        the resolution, index denotes the output-index which is taken '''
        feld = np.zeros((width,width))
        for x in range(0,width):
            for y in range(0,width):
                values = self.getValues(float(x)/(width-1) * factor + translation, float(y)/(width-1) * factor + translation)
                feld[x, y] = values[index]
        return feld
        
    def getFieldSymbols(self, factor, translation, width):
        ''' Like showField, but returns the index(e.g. 0, for nothing, 1 for stone, 2 for scissor, 3 for paper) '''
        feld = np.zeros((width,width))
        for x in range(0,width):
            for y in range(0,width):
                values = self.getValues(float(x)/(width-1) * factor + translation, float(y)/(width-1) * factor + translation)
                feld[x, y] = values.index(max(values))
        return feld.astype(int)
    
    def showField(self, factor, translation, width):
        ''' Shows the whole substrate by displaying different values for different
        maximum substrates '''
        feld = np.zeros((width,width))
        for x in range(0,width):
            for y in range(0,width):
                values = self.getValues(float(x)/(width-1) * factor + translation, float(y)/(width-1) * factor + translation)
                feld[x, y] = Cnpn_conf.sub_colors[values.index(max(values))]
        return feld
        
    
    def getFieldValues(self, factor, translation, width):
        ''' Computes for a given range all values, the field is multiplied
        with factor and translated by the given parameter, weight refers to
        the resolution, all values are returned '''
        feld = np.zeros((len(self.outputs), width,width))
        for x in range(0,width):
            for y in range(0,width):
                values = self.getValues(float(x)/(width-1) * factor + translation, float(y)/(width-1) * factor + translation)
                for v in range(len(self.outputs)):
                    feld[v, x, y] = values[v]
        return feld    
    
    def getField3D(self, factor, translation, width, index=0):
        ''' Computes for a given range all values, the field is multiplied
        with factor and translated by the given parameter, weight refers to
        the resolution, index denotes the output-index which is taken '''
        self.field = np.zeros((width,width, width))
        for x in range(0,width):
            tx = float(x)/(width-1) * factor + translation
            for y in range(0,width):
                ty = float(y)/(width-1) * factor + translation
                for z in range(0, width):
                    values = self.getValues(tx, ty, float(z)/(width-1) * factor + translation)
                    self.field[x, y, z] = values[index]
        return self.field     
        
    
    def checkForRecurrence(self):
        ''' Returns true if recurrence has been detected '''
        result = False
        for gen in self.outputs:
            self.reset()
            result = result | gen.checkRecurrence(-1)
        return result 
            
    def getCisNumber(self):
        ''' returns the total number of cis-elements '''
        number = 0
        for gen in self.genlist:
            number += gen.usedCis()
        return number
    
    def getCisWeight(self, cis_num):
        ''' Returns weight of cis-element cis_num '''
        gen_num = 0
        while (self.genlist[gen_num].usedCis() < cis_num):
            cis_num -= self.genlist[gen_num].usedCis()
            gen_num += 1
        
        return self.genlist[gen_num].cis[cis_num][1]
    
    def cleanGenome(self):
        ''' Resets all weightings of the basic genome to standard values '''
        for gen in self.genlist:
            gen.bias = 0.0;
            for cis in gen.cis:
                cis[1] = 0.0

        
        
    """//////////////////////////////////////
    Mutation and Crossover operation
    /////////////////////////////////////""" 

    def synapsis(self, other):
        ''' Aligns each gene of both chromosomes with each other '''
        code = []
        sgenlist = copy.deepcopy(self.genlist)
        ogenlist = copy.deepcopy(other.genlist)
        for gen in sgenlist:
            g = self.searchForHomologeGen(gen, ogenlist)
            if (isinstance(g, Gen)):
                code.append([gen, g])
                ogenlist.remove(g)
            else:
                code.append([gen, []])
        
        for g in ogenlist:
            code.append([[], g])
        return code
    
    def searchForHomologeCis(self, cis_elem, cis_list):
        ''' returns a homologe cis-element for a given cis-element
        (this is a cis-element with a gene with the same id '''
        for c in cis_list:
            if cis_elem[0].id == c[0].id:
                return c
        return []
    
    def gene_similarity(self, gene1, gene2):
        ''' compare the similarity between gene 1 and 2 '''
        diff_weight = abs(gene1.bias - gene2.bias)  # start with the bias of both genes
        num_weights = 1
        diff_cis = 0   
        
        cis_list = copy.deepcopy(gene2.cis)
        for cis in gene1.cis:
            c = self.searchForHomologeCis(cis, cis_list)
            ''' if c is empty, a corresponding cis-element in gene2 could not be found '''
            if len(c) == 0:
                diff_cis += 1
            else:
                diff_weight = diff_weight + abs(cis[1] - c[1]) + abs(cis[2] - c[2]) + abs(cis[3] - c[3])
                num_weights += 1
                cis_list.remove(c)   # remove this element in order to count the differences at the end
        ''' add the remaining cis-elements as differences '''
        diff_cis += len(cis_list)
        return diff_weight, num_weights, diff_cis
                
    
    def similarity(self, other):
        ''' measures the similarity between this genome and another one. 
        the similarity is the number of different genes and the difference in weightings '''
        
        ''' first, do synapsis in order to compare the genes '''
        synaptic_code = self.synapsis(other)
        
        ''' values to count '''
        diff_genes = 0.      # number of different genes
        diff_weights = 0.    # averaged difference between weights 
        num_weights = 0.     # number of weigths for computing the difference
        diff_cis = 0.        # number of differing cis-elements
        
        ''' count the number of different genes and measure the averaged difference 
        between the weightings, if they refer to the same genes '''
        for pairing in synaptic_code:
            ''' do we have a gene in both genomes '''
            if (isinstance(pairing[0], Gen) & (isinstance(pairing[1], Gen))):
                ''' compare both genes '''
                dw, nw, dc = self.gene_similarity(pairing[0], pairing[1])
                diff_weights += dw
                num_weights += nw
                diff_cis += dc
            else:
                ''' if not, count the number of differing genes '''  
                diff_genes += 1
        
        similarity = diff_genes * Cnpn_conf.sim_dg + (diff_weights / num_weights) * Cnpn_conf.sim_dw + diff_cis * Cnpn_conf.sim_dc
        return similarity

    def similarity_phenotype(self, other):
        p1 = self.showField(Cnpn_conf.display_area[0], Cnpn_conf.display_area[1], Cnpn_conf.display_area[2])
        p2 = other.showField(Cnpn_conf.display_area[0], Cnpn_conf.display_area[1], Cnpn_conf.display_area[2])
        return sum(sum(p1 == p2))
        

    def checkDownDependency(self, gene, side, synaptic_code):
        ''' checks down-dependencies for genes. If one gene only occurs in one 
        genome, and if it is used in the new genome, generated by crossover, 
        this gene should also be able to activate other genes, and not hang around
        unused '''
        
        ''' go through all genes on one side of the synaptic code '''
        for pairing in synaptic_code:
            ''' check for each cis-element whether it refers to gene '''
            if (isinstance(pairing[side], Gen)):
                for cis in pairing[side].cis:
                    ''' if so, add this cis-element to the corresponding other side 
                    of the synaptic code'''
                    if (cis[0].id == gene.id) & (isinstance(pairing[(side + 1) % 2], Gen)):
                        pairing[(side + 1) % 2].addCis(cis[0], cis[1], cis[2], cis[3])

    def crossover(self, other):
        ''' Performs crossover on this and a partner individual '''
        ''' First both chromosomes with homologous genes are matched ''' 
        synaptic_code = self.synapsis(other)
        
        ''' go through each pairing and perform crossover'''
        side = int(random() * 2)   # current side from which code is taken
        code = []
        for pairing in synaptic_code:
            ''' If a gen exists in both chromosomes, take one and test for crossover-change '''
            if (isinstance(pairing[0], Gen) & (isinstance(pairing[1], Gen))):
                code.append(pairing[side])
                ''' check for change of chromosome '''
                if (random() <= Cnpn_conf.crossover_rate):
                    side = (side + 1) % 2          
            else:
                ''' otherwise, take the Gen by chance '''
                ran = (random()*(self.fitness+other.fitness))
                if (ran<self.fitness):
                    if (isinstance(pairing[0], Gen)):
                        code.append(pairing[0])
                        self.checkDownDependency(pairing[0], 0, synaptic_code)
                else:
                    if (isinstance(pairing[1], Gen)):
                        code.append(pairing[1])
                        self.checkDownDependency(pairing[1], 1, synaptic_code)
                    
        new_gen = Genome(1)
        new_gen.adoptGenList(code)

        return new_gen
                    
        
    def mutate_AddCis(self):
        '''
        Selects a gene to add a cis-element
        '''
        ''' get a random number between a and b. a is len(self.inputs) and b is len(self.genlist) '''
        gen_num = int(random() * len(self.genlist ))
        
        new_cisgene = int(random() * (len(self.genlist) + len(self.inputs)))
        
        if (new_cisgene < len(self.inputs)):
            self.genlist[gen_num].addCis(self.inputs[new_cisgene], self.getRandom(), self.getRandom(1), self.getRandom())
        else:
            self.genlist[gen_num].addCis(self.genlist[new_cisgene - len(self.inputs)], self.getRandom(), self.getRandom(1), self.getRandom())
        
            
    def mutate_AddCisGene(self, innovation_id):
        '''
        Adds a cis-element along with a gene. The idea is, that a gene is activated by another, new gene
        '''
        aim_num = int(random() * len(self.genlist))   # any gene can be effected except the inputs
        start_num = int(random() * (len(self.genlist) + len(self.inputs))) # any gen can be a start gene
        
        ''' to avoid, that aim_num and start_num are equal, search for another start_num '''
        while ((aim_num+len(self.inputs)) == start_num):
            start_num = int(random() * (len(self.genlist) + len(self.inputs)))
            
        aim_gen = self.genlist[aim_num]
        
        ''' determine the start-gene '''
        if (start_num < len(self.inputs)):
            start_gen = self.inputs[start_num]
        else:
            start_num -= len(self.inputs)
            start_gen = self.genlist[start_num]

           
        new_gen = Gen(innovation_id)
        new_gen.func = ActiveFunction.af_list[int(random() * ActiveFunction.af_number)]
        new_gen.addCis(start_gen, self.getRandom(), self.getRandom(1), self.getRandom())
        aim_gen.addCis(new_gen, self.getRandom(), self.getRandom(1), self.getRandom())
        
        self.genlist.append(new_gen) 
    
    def mutate_AddGene(self, innovation_id):
        '''
        Adds a gene to the genome
        '''
        
        ''' In 50% of the cases a gene is added along with a cis '''
        if (random() < 0.5):
            # print('jep')
            self.mutate_AddCisGene(innovation_id)
            return
        
        ''' get a random number between a and b. a is len(self.inputs) and b is len(self.genlist) '''
        gen_num = 0
        ''' get a random cis-element in the selected gen '''
        cis_num = int(random() * self.getCisNumber())
        while (self.genlist[gen_num].usedCis()-1 < cis_num):
            cis_num -= self.genlist[gen_num].usedCis()
            gen_num += 1
        
        self.addGene(gen_num, cis_num, innovation_id)
        
    def addGene(self, gen_num, cis_num, innovation_id):
        ''' a connection between two genes is replaced by an intermediate gene. the
        connection weight is set to zero '''        
                
        ''' add the intermediate note '''
        new_gen = Gen(innovation_id)
        new_gen.func = ActiveFunction.af_list[int(random() * ActiveFunction.af_number)]
        new_gen.addCis(self.genlist[gen_num].cis[cis_num][0], self.getRandom(), self.getRandom(1), self.getRandom())
        self.genlist[gen_num].addCis(new_gen, self.getRandom(), self.getRandom(1), self.genlist[gen_num].cis[cis_num][3])
        
        ''' Disable the old connection => set weight to zero '''
        self.genlist[gen_num].cis[cis_num][3] = 0.0        
        
        self.genlist.append(new_gen)        
        
    def mutate_CisWeightContinues(self, cis_num):
        ''' Changes randomly the weight of a certain cis-item. Only a slight change
        is made '''
        ''' get a random number between a and b. a is len(self.inputs) and b is len(self.genlist) '''
        gen_num = 0
        ''' get a random cis-element in the selected gen '''
        while (self.genlist[gen_num].usedCis()-1 < cis_num):
            cis_num -= self.genlist[gen_num].usedCis()
            gen_num += 1
            
        ''' select a cis-value '''
        cis_value = int(random()*3)+1
        
        if (cis_value < 4):
            self.genlist[gen_num].cis[cis_num][cis_value] = self.genlist[gen_num].cis[cis_num][cis_value] + self.getRandomChange()
        else:
            self.mutate_Bias(self.genlist[gen_num])

        return [gen_num, cis_num]
    
    def mutate_DuplicateGene(self):
        ''' duplicates a gene and adjusts the output weights, so that no change in the phenotype occurs '''
        # gets a gen_number. every gene can be duplicated except input- and output-genes 
        gen_num = int(random()*(len(self.genlist)-len(self.outputs)) + len(self.outputs))
        gen = copy.deepcopy(self.genlist[gen_num])
        gen.id = int((time()+random()) * 1000)
        self.genlist.append(gen)
        
        # for each cis-element that uses the gen gen_num, half the weight and add the new gen
        for gene in self.genlist:
            for cis in gene.cis:
                # if the id of the cis-element equals the id of the gen_num-gen
                if (cis[0].id == self.genlist[gen_num].id):
                    # half the weight
                    cis[3] = cis[3] / 2
                    # add the duplicate
                    gene.addCis(gen, cis[1], cis[2], cis[3])
    
    def mutate_Bias(self, gene):
        gene.bias = gene.bias + self.getRandomChange()
    
    def mutate_GenFunction(self, gen_num):
        self.genlist[gen_num].func = ActiveFunction.af_list[int(random() * ActiveFunction.af_number)]    
    
    def searchForHomologeGen(self, gen, genlist):
        for h in genlist:
            if (gen.id == h.id):
                return h
        return False
    
    def changeAllCisWeights(self):
        ''' Changes all Cis-Weights '''
        for i in range(0, self.getCisNumber()):
            self.changeCisWeight(i)
    
    def changeCisWeight(self, cis_num):
        ''' Changes weight of cis-element cis_num '''
        gen_num = 0
        while (self.genlist[gen_num].usedCis()-1 < cis_num):
            cis_num -= self.genlist[gen_num].usedCis()
            gen_num += 1
        
        self.genlist[gen_num].cis[cis_num][1] = self.genlist[gen_num].cis[cis_num][1] + self.getRandom(0.) * Cnpn_conf.weight_change
        self.genlist[gen_num].cis[cis_num][2] = self.genlist[gen_num].cis[cis_num][2] + self.getRandom(0.) * Cnpn_conf.weight_change
        self.genlist[gen_num].cis[cis_num][3] = self.genlist[gen_num].cis[cis_num][3] + self.getRandom(0.) * Cnpn_conf.weight_change
    
    def exportGraph(self, filename):
        graph = GvGen()

        fd = open(filename, 'w')
        nodelist = []
        
        inputs = graph.newItem('Inputs')
        node_ids = []   # is used to address nodes in nodelist by id of the gene
        
        for g in self.inputs:
            if (g.id < len(self.labels)):
                node = graph.newItem(self.labels[g.id], inputs)
            else:
                node = graph.newItem(str(g.id), inputs)
            nodelist.append(node)
            node_ids.append(g.id)
            
        outputs = graph.newItem('Outputs')
        for g in self.outputs:
            if (g.id < len(self.labels)):
                node = graph.newItem(self.labels[g.id] + ' ' + str(g.func)[10:15], outputs)
            else:
                node = graph.newItem(str(g.id) + ' ' + str(g.func)[10:15], outputs)
            nodelist.append(node)
            node_ids.append(g.id)            
            
        for g in self.genlist[len(self.outputs):]:
            node = graph.newItem(str(g.id) + ' ' + str(g.func)[10:15])
            nodelist.append(node)
            node_ids.append(g.id)

            
        for g in self.genlist:
            for c in g.cis:
                if c[1] != 0.0:
                    graph.newLink(nodelist[node_ids.index(c[0].id)], nodelist[node_ids.index(g.id)], str(round(c[2]*100)/100))

        graph.dot(fd)
        fd.close()       
        
    def detectSingleGenes(self):
        ''' returns a list of genes that are not involved in production of the pattern '''
        self.reset()
        self.x.bias = 0.5
        self.y.bias = 0.5
        self.d.bias = 0.0
        #self.b.bias = 1
        
        t = []
        
        ''' compute output of each output-gen '''
        for output_gen in self.outputs:
            t.append(output_gen.getValue(-1))
            
        result = []
        ''' detect genes that are not involved in other genes '''
        for g in self.genlist:
            if len(g.callers) == 0:
                result.append(g)
        return result
        
  
      
  
'''  
f = open('gen.gen', 'rb')
g = pickle.load(f)

g.labels = ['x', 'y', 'd', 'b', 's', 'c', 'p', 'n']
g.exportGraph('test.dot')
g.detectSingleNodes()
g.genlist[6].callers
'''
  
'''g = Genome(1)
#g.exportGraph('graph.dot')
new_gen = Gen(g.nextGenId())
g.cleanGenome()
new_gen.func = ActiveFunction.sin

new_gen.addCis(g.x, g.getRandom())
g.n.addCis(new_gen, g.getRandom())

#matplotlib.pyplot.imshow(g.getField(2, -1, 100,0))              
#matplotlib.pyplot.show()

g.exportGraph('graph.dot')'''



'''g.mutate_AddGene(g.nextGenId())
g.exportGraph('graph.dot')
g.mutate_AddGene(g.nextGenId())
g.exportGraph('graph.dot')
g.mutate_AddGene(g.nextGenId())
g.exportGraph('graph.dot')'''


