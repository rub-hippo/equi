# -*- coding: utf-8 -*-
"""
Created on Thu May 22 09:39:23 2014

@author: martin
"""

import analyseEvolvability as aE
from matplotlib.pyplot import *
import numpy as np
import scipy.stats as stats
import glob
import pickle
import collections

data_path = '/home/martin/Documents/OE-HyperNEAT/'

def getNumOfSpecies():
    r = aE.getNumOfSpecies('/../../../../Documents/OE-HyperNEAT/regular6')
    figure()
    plot(r)
    
    
def evolvability():
    figure()
    y, x = aE.plotEvoOfSpecies(data_path + 'regular5', plot_data = False)
    plot(x, y, 'c')
    y, x = aE.plotEvoOfSpecies(data_path + 'regular6', plot_data = False)
    plot(x, y, 'b')
    y, x = aE.plotEvoOfSpecies(data_path + 'regular8', plot_data = False)
    plot(x, y, 'm')
    y, x = aE.plotEvoOfSpecies(data_path + 'regular9', plot_data = False)
    plot(x, y, 'r')
    y, x = aE.plotEvoOfSpecies(data_path + 'regular10', plot_data = False)
    plot(x, y, 'g')
        
    figure()
    y, x = aE.plotEvoOfSpecies(data_path + 'random1', plot_data = False)
    plot(x, y, 'b--')
    y, x = aE.plotEvoOfSpecies(data_path + 'random2', plot_data = False)
    plot(x, y, 'r--')
    y, x = aE.plotEvoOfSpecies(data_path + 'random3', plot_data = False)
    plot(x, y, 'g--')

def plotevoAlongSpecies(
        c1, e1, c2, e2, c3, e3,
        rc1, re1, rc2, re2, rc3, re3):
    #figure()
    #plot(e1[0], e1[1], '.b')
    #plot(e2[0], e2[1], '.r')
    #plot(e3[0], e3[1], '.g')
    
    group_difference = stats.ttest_ind(c1 + c2 + c3, rc1 + rc2 + rc3)
    print('Difference between regular and random: ' + str(group_difference[1]))
    
    figure()
    subplot(1,3,1)
    boxplot([c1, c2, c3, rc1, rc2, rc3])
    title('Correlation between\n evolvability and species-size')
    
    subplot(1,3,2)
    hist(c1 + c2 + c3, bins=15, range=(-1., 1.), 
         orientation='horizontal', color='0.5')
    hist(rc1 + rc2 + rc3, bins=15, range=(-1., 1.), 
         orientation='horizontal', color='0.75')
    title('Histogram of the\n correlation coefficients')
    
    subplot(1,3,3)
    plot(e1[0],c1, '.b')
    plot(e2[0],c2, '.r')
    plot(e3[0],c3, '.g')
    
    plot(re1[0],rc1, '*b')
    plot(re2[0],rc2, '*r')
    plot(re3[0],rc3, '*g')
    title('Species-age vs. correlation coefficient')


def evoAlongSpeciesRegular():
    corrs1, evo_length1 = aE.plotEvoAlongSpecies(data_path + 'regular6')
    corrs2, evo_length2 = aE.plotEvoAlongSpecies(data_path + 'regular9')
    corrs3, evo_length3 = aE.plotEvoAlongSpecies(data_path + 'regular10')

    return corrs1, evo_length1, corrs2, evo_length2, corrs3, evo_length3

def evoAlongSpeciesRandom():
    corrs1, evo_length1 = aE.plotEvoAlongSpecies(data_path + 'random1')
    corrs2, evo_length2 = aE.plotEvoAlongSpecies(data_path + 'random2')
    corrs3, evo_length3 = aE.plotEvoAlongSpecies(data_path + 'random3')
    
    return corrs1, evo_length1, corrs2, evo_length2, corrs3, evo_length3    
    
def evoAlongSpecies():
    c1, e1, c2, e2, c3, e3 = evoAlongSpeciesRegular()
    rc1, re1, rc2, re2, rc3, re3 = evoAlongSpeciesRandom()
    plotevoAlongSpecies(c1, e1, c2, e2, c3, e3, rc1, re1, rc2, re2, rc3, re3)
    
def flatten(x):
    if isinstance(x, collections.Iterable):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]
    
def figure_evolvability():    
    liste = sorted(glob.glob(data_path + '*of_species.equi'))
    figure()
    subplot(1,2,1)
    for result in liste[40:]:
        #figure()
        f = open(result)
        [y, x] = pickle.load(f)
        f.close()
        print(result, len(y), y[-1])
        plot(x, y)
        #title(result[43:45])
    title('Population-based fitness')
    ylim((0.0, 140.))
    xlim((0, 250))
    
    subplot(1,2,2)    
    for result in liste[20:40]:
        f = open(result)
        [y, x] = pickle.load(f)
        f.close()
        print(result, len(y), y[-1])
        plot(x, y)
    title('Random fitness')
    ylim((0.0, 140.))
    xlim((0, 250))
    

def getEvolvabilityMean(liste):
    regular = [[] for i in range(0, 250)]
    for result in liste: #figure()
        f = open(result)
        [y, x] = pickle.load(f)
        f.close()
        print result, len(y), y[-1]
        # save all values of all runs
        if len(y) >= 50:
            for i in range(0, len(x)):
                regular[int(x[i])].append(y[i])
    
# compute mean and std
    regular_result = [[], [], []] # x, y-mean, y-std
    for x, y in enumerate(regular):
        if (len(y) > 0) & ((x % 5) == 0):
            regular_result[0].append(x)
            regular_result[1].append(np.mean(y))
            regular_result[2].append(np.std(y))
    
    return regular_result, result

def figure_evolvability_mean():    
    liste = sorted(glob.glob(data_path + '*of_species.equi'))
    figure()
    
    random_result, random = getEvolvabilityMean(liste[20:40])
    plot(random_result[0], random_result[1], 'b', label='Random')
    errorbar(random_result[0],
             random_result[1], 
             yerr= random_result[2], 
             linestyle="None", 
             marker="None", 
             color="blue")
    
    regular_result, result= getEvolvabilityMean(liste[40:])    
    plot(regular_result[0], regular_result[1], 'g', label='Regular')
    errorbar(regular_result[0],
             regular_result[1], 
             yerr= regular_result[2], 
             linestyle="None", 
             marker="None", 
             color="green")
    
    legend(loc="best")   
        
    
def figure5():
    liste = sorted(glob.glob(data_path + '*along*.equi'))
    reg_c = []
    reg_e = []
    ran_c = []
    ran_e = []
    for result in liste[0:20]:
        print(result)
        f = open(result)
        [c, e] = pickle.load(f)
        f.close()
        ran_c.append(c)
        ran_e.append(e)
        
    for result in liste[40:]:
        print(result)
        f = open(result)
        [c, e] = pickle.load(f)
        f.close()
        reg_c.append(c)
        reg_e.append(e)
        
    figure()
    subplot(1,3,1)
    boxplot(reg_c + ran_c)
    plot([0, 40], [0, 0], '--k')
    xticks(np.arange(0, 40, 5))
    title('Correlation between\n evolvability and species-size')
    
    subplot(1,3,2)
    reg_c_flat = flatten(reg_c)
    ran_c_flat = flatten(ran_c)
    hist(reg_c_flat, bins=30, range=(-1., 0.8), 
         orientation='horizontal', color='0.5')
    hist(ran_c_flat, bins=30, range=(-1., 0.8), 
         orientation='horizontal', color='0.75')
    plot([0, 70], [0, 0], '--k')
    title('Histogram of the\n correlation coefficients')
    
    subplot(1,3,3)
    reg_evo = []
    ran_evo = []
    for e in reg_e:
        reg_evo = reg_evo + e[0]
        
    for e in ran_e:
        ran_evo = ran_evo + e[0]
        
        
    plot(reg_evo, reg_c_flat, '.b')
       
    plot(ran_evo, flatten(ran_c), '*k')
    plot([0, 250], [0, 0], '--k')
    title('Species-age vs. correlation coefficient')

    #figure()
    generations = [[] for i in range(260)]
    for i in range(len(reg_evo)):
        generations[reg_evo[i]].append(reg_c_flat[i])
    #boxplot(generations)
    mean_values = []
    for g in generations:
        if len(g) > 0:
            mean_values.append(np.mean(g))
            
    plot(np.unique(reg_evo), mean_values, '.r')
            
    group_difference = stats.ttest_ind(reg_c_flat, ran_c_flat)
    print('Difference between regular and random: ' + str(group_difference[1]))
                
    reg_means = [np.mean(r) for r in reg_c]
    ran_means = [np.mean(r) for r in ran_c]
    
    group_difference = stats.ttest_ind(reg_means, ran_means)
    print('Difference between regular and random means: ' + str(group_difference[1]))                
    return mean_values, reg_evo
            
        
def figure_evo_basics():
    figure()
    [c, e] = aE.plotEvoAlongSpecies(data_path + 'regular13')
    #title('Evolvability per species')
    #subplot(1,2,2)
    aE.plotEvoOfSpecies(data_path + 'regular13')
    title('Evolvability per species')
    
def plotPPVarEvo(reg_file, rand_file):
    reg = np.loadtxt(reg_file)
    reg = reg[reg != 0]
    rand = np.loadtxt(rand_file)
    rand = rand[rand != 0]
    #boxplot([reg, rand])
    #title("Correlation between evolvabilty and PP_var")
    #ylabel("Correlation coefficient")
    #xticks([1, 2], ["Regular", "Random"])
    #ylim(-1., 0.5)
    #figure()
    hist(rand, bins=20, range=(-1., 0.4), alpha = 0.5, label="Random")
    hist(reg, bins=20, range=(-1., 0.4), alpha = 0.5, label="Regular")
    hold(True)
    reg_mean = np.mean(reg)
    rand_mean = np.mean(rand)
    print("Reg mean: " + str(reg_mean))
    print("Rand mean: " + str(rand_mean))
    plot([reg_mean, reg_mean], [0, 12], color="g", linewidth=2.0,
         linestyle = "--")
    plot([rand_mean, rand_mean], [0, 12], color="b", linewidth=2.0,
         linestyle = "--")
    legend(loc="best")
    title("Distribution of correlation values")
    xlabel("Correlation coefficient")
    ylabel("Number of species")
    print( scipy.stats.ranksums(reg, rand) )
    # remove_border()
    return reg, rand


def plotPPMean(reg_file, rand_file):
    reg = np.loadtxt(reg_file, delimiter=";")
    reg = reg[reg[:,0] != 0,:]
    rand = np.loadtxt(rand_file, delimiter=";")
    rand = rand[rand[:,0] != 0,:] 
    hist(rand[:,1], bins=20, range=(-600., 1500), alpha = 0.5, label="Random")
    hist(reg[:,1], bins=20, range=(-600., 1500), alpha = 0.5, label="Regular")
    hold(True)
    title("Distribution of mean values")
    xlabel("Mean values")
    ylabel("Number of species")
        # remove_border()
    return reg, rand    
    

def getSpeciesSizes(list_reg):
    var_reg = []
    for i, file in enumerate(list_reg):
        print str(i) + '/' + str(len(list_reg))
        f = open(file, 'rb')
        m = pickle.load(f)
        spec_size = []
        for species in m.p.species:
            spec_size.append(len(species.genomes))
        
        var_reg.append(np.std(spec_size))
        f.close()
    return var_reg


def plotSpeciesSizeVar_plot(var_rand, var_reg):
    labels = ["Random", "Regular"]
    
    width = 0.5
    xlocations = np.concatenate(
        (np.zeros((len(var_rand),1)), 
         np.ones((len(var_reg),1)))) + width
    
    plot(xlocations,np.concatenate((var_rand, var_reg)), '*')
    #yticks(range(0, 8))
    xticks(np.array((0.5, 1.5)), labels)
    xlim(0, xlocations[-1]+width)
    title("Variance of species size")
    gca().get_xaxis().tick_bottom()
    gca().get_yaxis().tick_left()    
    #hist(var_reg)
    #hold(True)
    #hist(var_rand)

def plotSpeciesSizeVar():
    list_rand = sorted(glob.glob(data_path + 'lastEvos/crandom_rand*pop'))
    list_reg = sorted(glob.glob(data_path + 'lastEvos/cregular*pop'))
    
    var_rand = []
   
    var_reg = getSpeciesSizes(list_reg)
    var_rand = getSpeciesSizes(list_rand)
    
    plotSpeciesSizeVar_plot(var_rand, var_reg)
    
    return var_reg, var_rand

def plotBattleVarEvo():
    reg = np.loadtxt(data_path + 'ppvarevo_data_regular.txt', delimiter=";")
    reg = reg[reg[:,0] != 0,:]
    rand = np.loadtxt(data_path + 'ppvarevo_data_random_rand.txt', delimiter=";")
    rand = rand[rand[:,0] != 0,:]
    scatter(reg[:,0], reg[:,2], c='g', label="Regular")
    scatter(rand[:,0], rand[:,2], c='b', label="Random")
    
    
    xlabel("Variance in battle matrix")
    ylabel("Evolvability")
    legend(loc="best")
    
    
#figure_evolvability_mean()    
    
#evolvability()
#evoAlongSpecies()
#show()
    