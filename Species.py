'''
Created on 20.12.2011

@author: pyka
'''

import random
import Genome
import Cnpn_conf
from time import time
from operator import attrgetter


class Species(object):
    '''
    classdocs
    '''


    def __init__(self, representant = ''):
        '''
        Constructor
        '''
        self.id = 0
        self.fitness = 0.
        self.fitness_sum = 0.
        if representant == '':
            self.genomes = []
            self.representant = []            
        else:
            self.genomes = [representant]
            self.representant = representant
             
        
        
    def add(self, genome):
        self.genomes.append(genome)
        
    def setRepresentant(self, genome):
        self.representant = genome
        
    def selectRandomRep(self):
        ''' Select a random representant '''
        try:
            self.representant = random.choice(self.genomes)
        except:
            self.representant = []
            
    def detectFitness(self):
        ''' Computes the fitness of the whole species as average 
        over the fitness of each genome. 
        In this routine, fitness is also shared '''
        self.fitness_sum = 0.
        for genome in self.genomes:
            self.fitness_sum += genome.fitness
        self.fitness =  self.fitness_sum / len(self.genomes)
        return self.fitness
    
    def selectGenome(self):
        ''' selects a genome from the genome-list upon the fitness '''
        item = random.random() * self.fitness_sum
        ''' select the genome '''
        i = 0
        #print("Fitness: " + str(self.fitness_sum))
        #print("Item: " + str(item))
        #print("Length: " + str(len(self.genomes)))
        #print("=====================")
        
        while (self.genomes[i].fitness < item):
            #print(i)
            item -= self.genomes[i].fitness
            i += 1

        #print("======================")
        return self.genomes[i]
    
    def nextGeneration(self, num):
        ''' performs mutation and crossover to generate a next generation 
        num:     Number of offsprings
        '''
        ''' check whether we have genomes at all '''
        if (len(self.genomes) == 0):
            print("No individuals. Start with initialize()")
            return
        
        ''' if we have more than 3 genomes, sort them and throw all out but the best three '''
        self.genomes = sorted(self.genomes, key=attrgetter('fitness'), reverse=True)
        
        ''' select the best Cnpn_conf.ml_spec_best '''
        if (len(self.genomes)>Cnpn_conf.ml_spec_best):
            self.genomes = self.genomes[0:Cnpn_conf.ml_spec_best]
            self.fitness_sum = 0
            for g in self.genomes:
                self.fitness_sum += g.fitness
        
        ''' generate new species for next generation '''
        nspecies = Species(self.genomes[0])
        nspecies.id = self.id
        ''' generate num new offsprings '''
        resorts = []   # list of offsprings that are not similar enough for this species
        for i in range(1, num):
            parent1 = self.selectGenome()
            parent2 = self.selectGenome()
            offspring = parent1.crossover(parent2)
            
            ''' changes randomly the function of the gen '''
            for gen_num in range(0, len(offspring.genlist)):
                if (random.random()<Cnpn_conf.mutation_genfunction):
                    offspring.mutate_GenFunction(gen_num)
                    
            ''' adds randomly a cis-element '''
            if (random.random()<Cnpn_conf.mutation_addcis):
                offspring.mutate_AddCis()
            
            ''' adds randomly a gen '''
            if (random.random()<Cnpn_conf.mutation_addgen):
                offspring.mutate_AddGene(int((time()*random.random()) * 1000))
                #offspring.exportGraph('mutate_AddGene.dot')
                
            ''' duplicates genes with a certain probability '''
            if (random.random()<Cnpn_conf.mutation_duplicategen) & (len(offspring.genlist)>len(offspring.outputs)):
                offspring.mutate_DuplicateGene()
                #offspring.exportGraph('mutate_DuplicateGene.dot')
                
            ''' changes randomly the weightings of cis-elements '''
            for cis_num in range(0, offspring.getCisNumber()):
                if (random.random()<Cnpn_conf.mutation_continues):
                    offspring.mutate_CisWeightContinues(cis_num)
                
            ''' clean genome '''
            for gen in offspring.detectSingleGenes():
                offspring.genlist.remove(gen)
                
            resorts.append(offspring)
            
        ''' the species in the next generation consists just of the best element '''
        nspecies.genomes = [self.genomes[0]]
        self.genomes[0].fitness = 0
            
        return nspecies, resorts
        