import Gen
import Genome
import matplotlib.pyplot as plt

'''
Created on 19.12.2011

@author: pyka
'''


def modulateParameter(g, n, r):
    ''' modulates cis-element n of gene g in range r '''
    plt.figure()
    counter = 1 
    weight = g.getCisWeight(n)
    for i in r:
        g.changeCisWeight(n, i)
        feld = g.getField(2,-1, 100, 0)
        plt.subplot(1, len(r), counter)
        plt.imshow(feld)
        counter += 1
    g.changeCisWeight(n, weight)
        
def modulateAllParameters(g, r):
    for i in range(0, g.getCisNumber()-1):
        modulateParameter(g, i, r)
        
def generateRandomGenome():
    g = Genome.Genome(1)
    g.mutate_AddGene()
    g.mutate_AddGene()
    g.mutate_AddGene()
    g.mutate_AddCis()
    g.mutate_AddCis()
    g.mutate_AddCis()
    feld = g.getField(4, 2, 100)
    plt.imshow(feld)
    return g
        
def generateRandomGenomes():
    plt.figure
    for i in range(0, 16):
        g = Genome.Genome(1)
        g.mutate_AddGene()
        g.mutate_AddGene()
        g.mutate_AddGene()
        g.mutate_AddCis()
        g.mutate_AddCis()
        g.mutate_AddCis()
        feld = g.getField(4, 2, 100)
        
        plt.subplot(4, 4, i+1)
        plt.imshow(feld)
        
def addCisGene(genome, start_gene, end_gene, func):
    ''' adds a gene between start and end_gene and connects them with cis-elements '''
    new_gene = Gen.Gen(genome.nextGenId())
    new_gene.func = func
    
    new_gene.addCis(start_gene, 1.)
    end_gene.addCis(new_gene, 1.)
    genome.genlist.append(new_gene)

        