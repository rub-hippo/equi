'''
A class which simulates an object in 3d-space  

@author: pyka
'''

from Population import Population
from Genome import Genome

import matplotlib.pyplot as plt
import Cnpn_conf
import random
import pickle
import Species
import numpy as np
from operator import attrgetter
import shutil
import os

''' some constants that are used to fastly determine which substrate wins (deprecated)
in the battle '''
g_neutral = 0;
g_stone = 1;
g_scissor = 2;
g_paper = 3;

''' g_rules is an array, where g_rules[x, y] returns, whether x would win (1) 
or not (0) '''
g_rules = [[0, -1, -1, -1],    # for neutral
           [1, 0, 1, -1],    # for stone
           [1, -1, 0, 1],    # for scissor
           [1, 1, -1, 0]]    # for paper

config_file = '/home/martin/Dropbox/Python/OE-HyperNEAT/src/Cnpn_conf.py'

class ModelSearch():
    '''
    This class represents a population of Genomes and provides all methods for crossover, mutation and selection
    '''

    def __init__(self, innovation=0):
        self.p = Population(0)
        self.p.initialize(Cnpn_conf.ml_v)
        self.collectAllGenomes()
        self.all_fitness = 0    # fitness over all individuals
        self.max_fitness = 0    # best fitness in one run
        # self.genomelist = []

        self.x = 0   # counter-variable to see, how many genomes have been tested already 
        
        
    def startEA(self, generations=5, folder='./', random_fit=False, noise=False, start=0):
        ''' performs an initial search for improvable individuals '''
        #self.initialEvolution(10)
        if start > 0:
            f = open(folder + "pop%03d.pop" % start, 'rb')
            self.p = pickle.load(f)
            f.close()
            
        if start == 0:            
            ''' First of all copy the configuration-file to remember the exact setting '''
            if not os.path.exists(folder):
                os.makedirs(folder)
            shutil.copyfile(config_file, folder + 'Cnpn_conf.py')
        
        ''' starts the evolutionary algorithm for a given number of generations '''
        for x in range(start, generations):
            print('Generation ' + str(x))
            self.imsave_Population(folder + "test%03d" % x + ".png")
            self.save(folder + "pop%03d.pop" % x )            
            
            if (random_fit==False):
                self.measureFitness(noise)
            else:
                for s in self.p.species:
                    for g in s.genomes:
                        g.fitness = int(random.random() * 1000)
                for species in self.p.species:
                    species.detectFitness()        
            

            self.save_Fitness(folder + "test%03d" % x + ".txt")            
            self.save(folder + "pop%03d.pop" % x )
            self.generateNextGeneration()
            
    def initialEvolution(self, required = 10):
        ''' starts a fast search across various genome configurations until 
        genomes are found which can be improved 
        we did not implemented novelty search because we did not want to answer
        the question how many different solutions exist at this point '''
        found = 0
        while (found < required):
            self.p.addGenome(Genome(1), False)  # add each genome to the same species 
            found += 1

    def collectAllGenomes(self):
        ''' writes all genomes from all species into one list '''
        self.genomelist = []
        for species in self.p.species:
            for genome in species.genomes:
                self.genomelist.append(genome)
            
    def measureFitness(self, noise=False):
        ''' measures the fitness for each individual through direct competition,
        it receives the generation number for saving an image '''
        self.all_fitness = 0
        self.max_fitness = 0
        self.x = 0
        
        ''' collect all genomes to operate on the list of genomes directly '''
        self.collectAllGenomes()
        
        ''' collect all phenotypes '''
        phenos = []

        print("Create all phenotypes")
        for genome in self.genomelist:
            phenos.append(genome.getFieldSymbols(
                                                Cnpn_conf.display_area[0], 
                                                Cnpn_conf.display_area[1], 
                                                Cnpn_conf.display_area[2]))
        
        #if (random):
        #    fitness_vals = np.zeros((len(self.genomelist), 1))
        
        for x in range(0, len(self.genomelist)-1):
            print(x)
            if (noise):
                opponents = range(len(self.genomelist))
                random.shuffle(opponents)
                opponents = opponents[0:Cnpn_conf.ml_battles]
            else:
                opponents = range(x+1, len(self.genomelist))
                
            for y in opponents:
                fit1 = self.battle(phenos[x], phenos[y])
                #print(y, fit1)
                
                ''' add fitness-value to the genomes '''
                #if (random):
                #    fitness_vals[x] = self.genomelist[x].fitness + fit1;
                #    fitness_vals[y] = self.genomelist[y].fitness - fit1;
                #else:
                self.genomelist[x].fitness = self.genomelist[x].fitness + fit1
                if (noise == False):
                    self.genomelist[y].fitness = self.genomelist[y].fitness - fit1

        ''' adjust all fitness-values so that fitness reflects a fraction of the 
        maximal achievable fitness value '''
        
        #if (random):
        #    fitness_vals = np.random.permutation(fitness_vals)
        #    for i in range(len(fitness_vals)):
        #        fitness_vals[i] = fitness_vals[i] + (len(self.genomelist)-1) * (Cnpn_conf.display_area[2] ** 2);
        #        self.genomelist[i].fitness = fitness_vals[i]
        #else:
        if (noise):
            for g in self.genomelist:
                g.fitness = g.fitness + Cnpn_conf.ml_battles * (Cnpn_conf.display_area[2] ** 2);
        else:
            for x in range(len(self.genomelist)):
                g = self.genomelist[x]
                g.fitness = g.fitness + (len(self.genomelist)-1) * (Cnpn_conf.display_area[2] ** 2);
                #g.fitness = g.fitness / (3. / self.getNumberOfSubstrates(phenos[x]))

        
        print('Number species: ' + str(len(self.p.species)))
        for species in self.p.species:
            species.detectFitness()
            
            
    def getNumberOfSubstrates(self, p):
        ''' Determines for a given phenotype the number of substrates it express. '''
        u = np.unique(p)
        ''' the substrate zero does not play any role '''
        if (u[0] == 0):           
            return max(len(u)-1,1)
        else:
            return len(u)
            
    
    def battle(self, p1, p2):
        ''' determines the fitness of both genomes according
        to their fitness in the phenotype '''
        
        fit1 = 0 # fitness of g1 (for g2 it is -fit1)
        for x in range(Cnpn_conf.display_area[2]):
            for y in range(Cnpn_conf.display_area[2]):
                ''' get the list of values for a particular coordinate '''
                #s1 = [p1[g_neutral, x, y], p1[g_stone, x, y], p1[g_scissor, x, y], p1[g_paper, x, y]]      
                #s2 = [p2[g_neutral, x, y], p2[g_stone, x, y], p2[g_scissor, x, y], p2[g_paper, x, y]]
                ''' apply the game-rules to the substrates with the highest value ''' 
                fit1 = fit1 + g_rules[p1[x, y]][p2[x, y]]
        
        ''' returns the minimum because this is needed to adjust later on all fitness values '''
        return fit1
    
    
    def getOverallFitness(self):
        ''' Returns the sum of all fitness values across all species '''
        fit_all_species = 0
        for species in self.p.species:
            fit_all_species += species.fitness
        return fit_all_species
                
    
    def generateNextGeneration(self):
        ''' selects a few parents and generates the next generation '''
        
        ''' first, detect the sum of the averaged fitness over all species '''
        fit_all_species = 0
        for species in self.p.species:
            fit_all_species += species.fitness
        
        print('Overall fitness: ' + str(fit_all_species / len(self.p.species)))
        
        ''' when fitness is zero then the chances for selection should be equal 
        for any genome, so that every fitness value to 1 '''
        if fit_all_species == 0:
            for species in self.p.species:
                for genome in species.genomes:
                    genome.fitness = 1
                    fit_all_species += 1                
        
        ''' generate a new list of species '''
        rest = []
        ''' new species list '''
        nspecies = []
        for species in self.p.species:
            ''' number of offsprings for this species '''
            num_next = int(round(species.fitness / fit_all_species * Cnpn_conf.ml_v))
            ''' only, if at least two offsprings should be generated it makes sense
            to continue this species '''
            if num_next > 1:
                new_species, resorts = species.nextGeneration(num_next)
                nspecies.append(new_species)
                ''' add the remaining genomes to the rest '''
                for genome in resorts:
                    rest.append(genome)

        ''' replace species list '''
        self.p.species = nspecies

        ''' add all new genomes to the species '''
        for genome in rest:
            self.p.addGenome(genome)

        ''' remove all species with less than two genomes '''
        x = 0
        while x < len(self.p.species):
            if (len(self.p.species[x].genomes) < 2):
                self.p.species.remove(self.p.species[x])
            else:
                x += 1
        
    def printFitness(self):
        ''' Print fitness of all individuals '''
        i = 0
        for g in self.genomelist:
            print(str(i) + ': ' + str(g.fitness))
            i += 1
            
    def computeSimilarity(self):
        ''' computes similarity between all genomes '''
        similarity = np.zeros( (len(self.genomelist), len(self.genomelist)) )
                      
        for x in range(0, len(self.genomelist)-1):
            print(x)
            for y in range(x+1, len(self.genomelist)):
                sim = self.genomelist[x].similarity_phenotype(self.genomelist[y])
                similarity[x, y] = sim
                similarity[y, x] = sim
                
        return similarity
                      
            
    def plotGenomes(self, x, y):
        ''' Plots all genomes in genomelist. x and y denote the number of plots in rows and columns
        index denotes the output-variable, which should be plotted '''
        plt.figure();
        counter = 1
        for gen in self.genomelist:
            field = gen.showField(Cnpn_conf.display_area[0], Cnpn_conf.display_area[1], Cnpn_conf.display_area[2])
            plt.subplot(x, y, counter)
            plt.imshow(field, vmin=0, vmax=1)
            counter += 1         
            
    def save(self, filename):
        ''' saves the current generation '''
        f = open(filename, 'w')
        pickle.dump(self, f)
        f.close()               
        
    def save_Fitness(self,filename):
        ''' Reports all fitness values and important measures for the population in a textfile '''
        f = open(filename, 'w')
        
        elems = int(np.lib.scimath.sqrt(Cnpn_conf.ml_v))
        
        x = 0
        y = 0
        for s in self.p.species:
            for genome in s.genomes:
                if (y < elems):
                    f.write(str(genome.fitness).zfill(6) + '   ')
                
                x += 1
                if (x == elems):
                    f.write('\n')
                    x = 0
                    y += 1
                    
        f.write('\n\n\n')
        
        x = 0
        for s in self.p.species:
            f.write(str(s.id) + ': ' + str(len(s.genomes)) + '  F:' + str(s.fitness) + '\n')
            s.genomes = sorted(s.genomes, key=attrgetter('fitness'), reverse=True)
            for g in s.genomes:
                    f.write(str(g.fitness).zfill(6) + '/' + str(len(g.genlist)) + '  ')
            f.write('\n\n')
            x += 1
            
        f.close()
        
        
    def imsave_Population(self, filename):
        ''' saves the whole population as image, in contrast to imsave_phenotypes, this
        procedure calculates the phenotypes from scratch '''    
        elems = int(np.lib.scimath.sqrt(Cnpn_conf.ml_v))
        feld = np.zeros((Cnpn_conf.display_area[2]*elems, Cnpn_conf.display_area[2]*elems))
        x = 0
        y = 0
        for s in self.p.species:
            for genome in s.genomes:
                if (y < elems):
                    field = genome.showField(Cnpn_conf.display_area[0], 
                                             Cnpn_conf.display_area[1], 
                                             Cnpn_conf.display_area[2])
                    feld[(x*Cnpn_conf.display_area[2]):((x+1)*Cnpn_conf.display_area[2]),
                         (y*Cnpn_conf.display_area[2]):((y+1)*Cnpn_conf.display_area[2])] = field
                
                x += 1
                if (x == elems):
                    x = 0
                    y += 1
                    
                    
        plt.imsave(filename, feld, vmin=0, vmax=1)
        
    def imsave_Phenotypes(self, filename, phenotypes):
        ''' saves the whole population as image, it receives the phenotypes computed
        in self.measureFitness '''    
        elems = int(np.lib.scimath.sqrt(Cnpn_conf.ml_v))
        feld = np.zeros((Cnpn_conf.display_area[2]*elems, Cnpn_conf.display_area[2]*elems))
        x = 0
        y = 0
        for s in self.p.species:
            for genome in s.genomes:
                feld[(x*Cnpn_conf.display_area[2]):((x+1)*Cnpn_conf.display_area[2]),
                     (y*Cnpn_conf.display_area[2]):((y+1)*Cnpn_conf.display_area[2])] = phenotypes[x+y*10]
                
                x += 1
                if (x == elems):
                    x = 0
                    y += 1
                    
        plt.imsave(filename, feld, vmin=0, vmax=1)     
        
    def measureFitnessOfTwoSpecies(self,s1, s2):
        ''' Compares fitness of two species '''
        result = 0
        for g1 in s1.genomes:
            for g2 in s2.genomes:
                p1 = g1.getFieldValues( Cnpn_conf.display_area[0], 
                                        Cnpn_conf.display_area[1], 
                                        Cnpn_conf.display_area[2])
                p2 = g2.getFieldValues( Cnpn_conf.display_area[0], 
                                        Cnpn_conf.display_area[1], 
                                        Cnpn_conf.display_area[2])
                result = result + self.battle(p1,p2)
        return result
        
    def measurePairWiseSpeciesFitness(self):
        ''' Computes a fitness-matrix for each pair of species '''
        
        temp = Cnpn_conf.display_area[2]
        Cnpn_conf.display_area[2] = 20
        fitness = np.zeros( (len(self.p.species), len(self.p.species)) )
        for x in range(0, len(self.p.species)-1):
            print(x)
            for y in range((x+1),len(self.p.species)):
                fitness[x, y] = self.measureFitnessOfTwoSpecies(self.p.species[x], self.p.species[y])
                fitness[y, x] = fitness[x, y]
                
                
        Cnpn_conf.display_area[2] = temp
        return fitness
        
    
#f = open('./regular/pop027.pop')
#m = pickle.load(f)
#f.close()
#m = ModelSearch()
#m.startEA(50)
#m.measureFitness()
#m.generateNextGeneration()

#fit_all_species = 0
#for species in m.p.species:
#    fit_all_species += species.fitness
#
#print('Overall fitness: ' + str(fit_all_species / len(m.p.species)))
#
#''' when fitness is zero then the chances for selection should be equal 
#for any genome, so that every fitness value to 1 '''
#if fit_all_species == 0:
#    for species in m.p.species:
#        for genome in species.genomes:
#            genome.fitness = 1
#            fit_all_species += 1                
#
#''' generate a new list of species '''
#rest = []
#''' new species list '''
#nspecies = []
#for species in m.p.species:
#    ''' number of offsprings for this species '''
#    num_next = int(round(species.fitness / fit_all_species * Cnpn_conf.ml_v))
#    print(num_next)
#    ''' only, if at least two offsprings should be generated it makes sense
#    to continue this species '''
#    if num_next > 1:
#        new_species, resorts = species.nextGeneration(num_next)
#        nspecies.append(new_species)
#        ''' add the remaining genomes to the rest '''
#        for genome in resorts:
#            rest.append(genome)
#
#''' replace species list '''
#m.p.species = nspecies
#
#
#def AddGenome(m, genome):
#    ''' add it to a species or generate a new one '''
#    res = Cnpn_conf.display_area[2]**2
#    for species in m.p.species:
#        sim = genome.similarity_phenotype(species.representant)
#        print("Population.addGenome - Distance is " + str(float(sim)/res))
#        if (float(sim)/res)>Cnpn_conf.sim_threshold_phenotype:
#            print("Add to species")
#            species.genomes.append(genome)
#            return
#        
#    ''' if no species with similarity to genome could be found, create a new one,
#    but only, if the maximal number of species is not exceeded '''
#    if (len(m.p.species) < Cnpn_conf.ml_max_species):
#        species = Species.Species(genome)
#        species.id = m.p.nextID()
#        m.p.species.append(species)
#        print("Create new Species")
#    else:
#        m.p.species[-1].genomes.append(genome)
#        print("Add to last species")
#
#
#''' add all new genomes to the species '''
#x = 0
#for genome in rest:
#    print(x)
#    AddGenome(m, genome)
#    x += 1
#    
#
#''' remove all species with less than two genomes '''
#x = 0
#while x < len(m.p.species):
#    if (len(m.p.species[x].genomes) < 2):
#        m.p.species.remove(m.p.species[x])
#    else:
#        x += 1
#        
#        
#        
#        
#        
#        
#        
#
