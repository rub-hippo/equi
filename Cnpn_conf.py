'''
Created on 17.12.2011

@author: pyka
'''

display_area = [1., 0., 40]     # display-area for field. first number is factor, second is translation, third is number of pixels per axis
iterations = 3                  # number of iterations for the computation of the gen-value
weight_range = [0., 1.]         # first number is mean, second is std for gauss-random-values of weight-changes
weight_change = 0.10
#continues_change = [0., 0.02]   # range of continues modification of cis-weights
continues_change = [0., 0.05]   # range of continues modification of cis-weights
sub_colors = [0, 0.25, 0.5, 1]  # colors 

ax_length = 0.3             # initialization value for axon length
ax_angle = 0.3              # initialization value for axon angle (0-1 ranges from 0 to pi/2 => 90 degree)
ax_density = 1.0            # initialization value for axon density (1=100%)
ax_weight = 1.0             # initialization value for axon weight 

den_length = 0.3            # initialization value for dendrite length
den_angle = 0.3             # initialization value for dendrite angle (0-1 ranges from 0 to pi/2 => 90 degree)
den_density = 1.0           # initialization value for dendrite density (1=100%)
den_weight = 1.0            # initialization value for dendrite weight 

crossover_rate = 0.1        # absolute crossover change rate per gen
mutation_continues = 0.70   # P for continues weight change per cis-element
mutation_addgen = 0.05      # P for adding a gen per genome
mutation_addcis = 0.04      # P for adding a cis per genome 
mutation_genfunction = 0.01 # P for change in gen-function 
mutation_duplicategen = mutation_addgen # duplicates a gene

''' Similarity-values '''
sim_dg  = 1                 # weighting for differences in genes
sim_dw  = 8                 # weighting for differences in weightings and biases
sim_dc  = 1                 # weighting for differences in cis-elements
sim_threshold = 4.5         # threshold for similarity
sim_threshold_phenotype = 0.80 # threshold for phenotype similarity

''' Network configuration '''
octree_depth = 3            # depth of the octree for 3d-simualion
octree_min = (display_area[1], display_area[1], display_area[1])
octree_max = (display_area[0] + display_area[1], display_area[0] + display_area[1], display_area[0] + display_area[1])

coll_detail = 20            # collision detection between two cones is done by approximation. this number denotes, how many points of the dendritic vector will be checked for collision with the others neurons cone

ml_v = 400                  # model search population size    #ml_u = 10                   # model search selection size
ml_spec_best = 20           # how many best individuals should be used in one species
ml_dur = 7.                 # seconds of simulation time
ml_feld = 'feld.data'       # filename of the data (food and obstacle-field)
ml_max_species = 20         # maximal number of species
ml_battles = 50             # if noise is activated, this variable determines the number of random battles each individual should have

mod_thresh = 0.5            # threshold, at which modulatory influence is assumed

weight_factor = 125         # factor that is multiplied with the weight for the connection matrices 